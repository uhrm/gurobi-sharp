
//  
//  Const.cs
//  gurobi-sharp
//
//  Copyright 2009-2024 Markus Uhr <uhrmar@gmail.com>.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in
//        the documentation and/or other materials provided with the
//        distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

using System;

namespace Grb;

public enum ConstrSense : byte
{
    LessEqual    = (byte)'<',
    GreaterEqual = (byte)'>',
    Equal        = (byte)'='
}

public enum VariableType : byte
{
    Continuous = (byte)'C',
    Binary     = (byte)'B',
    Integer    = (byte)'I',
    Semicont   = (byte)'S',
    Semiint    = (byte)'N'
}

public enum VariableStatus
{
    Basic         =  0,
    NonbasicLower = -1,
    NonbasicUpper = -2,
    Superbasic    = -3
}

public enum ConstraintStatus
{
    Basic    =  0,
    Nonbasic = -1,
}

public enum ModelSense
{
    Min =  1,
    Max = -1
}

public enum ModelStatus
{
    Loaded         =  1,
    Optimal        =  2,
    Infeasible     =  3,
    InfOrUnbounded =  4,
    Unbounded      =  5,
    CutOff         =  6,
    IterationLimit =  7,
    NodeLimit      =  8,
    TimeLimit      =  9,
    SolutionLimit  = 10,
    Interrupted    = 11,
    Numeric        = 12
}

public enum SosType
{
    Type1 = 1,
    Type2 = 2
}

public enum GeneralConstraint
{
    Max       =  0,
    Min       =  1,
    Aab       =  2,
    And       =  3,
    Or        =  4,
    Norm      =  5,
    Indicator =  6,
    Pwl       =  7,
    Poly      =  8,
    Exp       =  9,
    ExpA      = 10,
    Log       = 11,
    LogA      = 12,
    Pow       = 13,
    Sin       = 14,
    Cco       = 15,
    Tan       = 16,
    Logistic  = 17
}
