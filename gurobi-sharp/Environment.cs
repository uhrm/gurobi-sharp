
//  
//  Environment.cs
//  gurobi-sharp
//
//  Copyright 2009-2024 Markus Uhr <uhrmar@gmail.com>.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in
//        the documentation and/or other materials provided with the
//        distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

using System;

namespace Grb;

public sealed class Environment : GurobiObject
{
    // environment construction
    
    private static IntPtr LoadEnv(string? logfile)
    {
        var errno = NativeMethods.GRBloadenv(out var ptr, logfile);
        if (errno != 0) {
            throw new GurobiException(errno, $"Error creating environment (error {errno}).");
        }
        return ptr;
    }
    
    internal Environment(IntPtr ptr) : base(ptr, false) { }
    
    public Environment() : this(null) { }
    
    public Environment(string? logfile) : base(LoadEnv(logfile), true) { }
    
    ~Environment()
    {
        Dispose(false);
    }
    
    // IDisposable stuff
    
    internal override void FreeRef()
    {
        NativeMethods.GRBfreeenv(ObjRef);
    }
    
    // error handling
    
    private void GrbErr(int errno)
    {
        if (errno != 0) {
            var errormsg = NativeMethods.GRBgeterrormsg(ObjRef);
            throw new GurobiException(errno, $"Gurobi error {errno}: {errormsg}");
        }
    }

    private int GetIntParam(nint env, string name)
    {
        GrbErr(NativeMethods.GRBgetintparam(env, name, out var i));
        return i;
    }

    private void SetIntParam(nint env, string name, int value)
    {
        GrbErr(NativeMethods.GRBsetintparam(env, name, value));
    }

    private double GetDblParam(nint env, string name)
    {
        GrbErr(NativeMethods.GRBgetdblparam(env, name, out var i));
        return i;
    }

    private void SetDblParam(nint env, string name, double value)
    {
        GrbErr(NativeMethods.GRBsetdblparam(env, name, value));
    }

    private string? GetStrParam(nint env, string name)
    {
        GrbErr(NativeMethods.GRBgetstrparam(env, name, out var i));
        return i;
    }

    private void SetStrParam(nint env, string name, string? value)
    {
        GrbErr(NativeMethods.GRBsetstrparam(env, name, value));
    }
    
    // termination parameters
    
    public int BarIterLimit
    {
        get => GetIntParam(ObjRef, "BarIterLimit");
        set => SetIntParam(ObjRef, "BarIterLimit", value);
    }
    
    public double Cutoff
    {
        get => GetDblParam(ObjRef, "Cutoff");
        set => SetDblParam(ObjRef, "Cutoff", value);
    }
    
    public double IterationLimit
    {
        get => GetDblParam(ObjRef, "IterationLimit");
        set => SetDblParam(ObjRef, "IterationLimit", value);
    }
    
    public double NodeLimit
    {
        get => GetDblParam(ObjRef, "NodeLimit");
        set => SetDblParam(ObjRef, "NodeLimit", value);
    }
    
    public int SolutionLimit
    {
        get => GetIntParam(ObjRef, "SolutionLimit");
        set => SetIntParam(ObjRef, "SolutionLimit", value);
    }
    
    public double TimeLimit
    {
        get => GetDblParam(ObjRef, "TimeLimit");
        set => SetDblParam(ObjRef, "TimeLimit", value);
    }
    
    // tolerances
    
    public double BarConvTol
    {
        get => GetDblParam(ObjRef, "BarConvTol");
        set => SetDblParam(ObjRef, "BarConvTol", value);
    }
    
    public double FeasibilityTol
    {
        get => GetDblParam(ObjRef, "FeasibilityTol");
        set => SetDblParam(ObjRef, "FeasibilityTol", value);
    }
    
    public double IntFeasTol
    {
        get => GetDblParam(ObjRef, "IntFeasTol");
        set => SetDblParam(ObjRef, "IntFeasTol", value);
    }
    
    public double MarkowitzTol
    {
        get => GetDblParam(ObjRef, "MarkowitzTol");
        set => SetDblParam(ObjRef, "MarkowitzTol", value);
    }
    
    public double MipGap
    {
        get => GetDblParam(ObjRef, "MIPGap");
        set => SetDblParam(ObjRef, "MIPGap", value);
    }
    
    public double MipGapAbs
    {
        get => GetDblParam(ObjRef, "MIPGapAbs");
        set => SetDblParam(ObjRef, "MIPGapAbs", value);
    }
    
    public double OptimalityTol
    {
        get => GetDblParam(ObjRef, "OptimalityTol");
        set => SetDblParam(ObjRef, "OptimalityTol", value);
    }
    
    public double PSDTol
    {
        get => GetDblParam(ObjRef, "PSDTol");
        set => SetDblParam(ObjRef, "PSDTol", value);
    }

    // simplex parameters
    
    public int InfUnbdInfo
    {
        get => GetIntParam(ObjRef, "InfUnbdInfo");
        set => SetIntParam(ObjRef, "InfUnbdInfo", value);
    }
    
    public int NormAdjust
    {
        get => GetIntParam(ObjRef, "NormAdjust");
        set => SetIntParam(ObjRef, "NormAdjust", value);
    }
    
    public double ObjScale
    {
        get => GetDblParam(ObjRef, "ObjScale");
        set => SetDblParam(ObjRef, "ObjScale", value);
    }
    
    public double PerturbValue
    {
        get => GetDblParam(ObjRef, "PerturbValue");
        set => SetDblParam(ObjRef, "PerturbValue", value);
    }
    
    public int Quad
    {
        get => GetIntParam(ObjRef, "Quad");
        set => SetIntParam(ObjRef, "Quad", value);
    }
    
    public int ScaleFlag
    {
        get => GetIntParam(ObjRef, "ScaleFlag");
        set => SetIntParam(ObjRef, "ScaleFlag", value);
    }
    
    public int Sifting
    {
        get => GetIntParam(ObjRef, "Sifting");
        set => SetIntParam(ObjRef, "Sifting", value);
    }
    
    public int SiftMethod
    {
        get => GetIntParam(ObjRef, "SiftMethod");
        set => SetIntParam(ObjRef, "SiftMethod", value);
    }
    
    public int SimplexPricing
    {
        get => GetIntParam(ObjRef, "SimplexPricing");
        set => SetIntParam(ObjRef, "SimplexPricing", value);
    }

    // barrier parameters
    
    public int BarCorrectors
    {
        get => GetIntParam(ObjRef, "BarCorrectors");
        set => SetIntParam(ObjRef, "BarCorrectors", value);
    }
    
    public int BarOrder
    {
        get => GetIntParam(ObjRef, "BarOrder");
        set => SetIntParam(ObjRef, "BarOrder", value);
    }
    
    public int Crossover
    {
        get => GetIntParam(ObjRef, "Crossover");
        set => SetIntParam(ObjRef, "Crossover", value);
    }
    
    public int CrossoverBasis
    {
        get => GetIntParam(ObjRef, "CrossoverBasis");
        set => SetIntParam(ObjRef, "CrossoverBasis", value);
    }

    // MIP parameters
    
    public int BranchDir
    {
        get => GetIntParam(ObjRef, "BranchDir");
        set => SetIntParam(ObjRef, "BranchDir", value);
    }

    public double Heuristics
    {
        get => GetDblParam(ObjRef, "Heuristics");
        set => SetDblParam(ObjRef, "Heuristics", value);
    }
    
    public double ImproveStartGap
    {
        get => GetDblParam(ObjRef, "ImproveStartGap");
        set => SetDblParam(ObjRef, "ImproveStartGap", value);
    }
    
    public double ImproveStartTime
    {
        get => GetDblParam(ObjRef, "ImproveStartTime");
        set => SetDblParam(ObjRef, "ImproveStartTime", value);
    }
    
    public double MinRelNodes
    {
        get => GetDblParam(ObjRef, "MinRelNodes");
        set => SetDblParam(ObjRef, "MinRelNodes", value);
    }
    
    public double MIPFocus
    {
        get => GetDblParam(ObjRef, "MIPFocus");
        set => SetDblParam(ObjRef, "MIPFocus", value);
    }
    
    public string? NodefileDir
    {
        get => GetStrParam(ObjRef, "NodefileDir");
        set => SetStrParam(ObjRef, "NodefileDir", value);
    }
    
    public double NodefileStart
    {
        get => GetDblParam(ObjRef, "NodefileStart");
        set => SetDblParam(ObjRef, "NodefileStart", value);
    }
    
    public int NodeMethod
    {
        get => GetIntParam(ObjRef, "NodeMethod");
        set => SetIntParam(ObjRef, "NodeMethod", value);
    }
    
    public int PumpPasses
    {
        get => GetIntParam(ObjRef, "PumpPasses");
        set => SetIntParam(ObjRef, "PumpPasses", value);
    }
    
    public int RINS
    {
        get => GetIntParam(ObjRef, "RINS");
        set => SetIntParam(ObjRef, "RINS", value);
    }
    
    public int SolutionNumber
    {
        get => GetIntParam(ObjRef, "SolutionNumber");
        set => SetIntParam(ObjRef, "SolutionNumber", value);
    }
    
    public int SubMIPNodes
    {
        get => GetIntParam(ObjRef, "SubMIPNodes");
        set => SetIntParam(ObjRef, "SubMIPNodes", value);
    }
    
    public int Symmetry
    {
        get => GetIntParam(ObjRef, "Symmetry");
        set => SetIntParam(ObjRef, "Symmetry", value);
    }
    
    public int VarBranch
    {
        get => GetIntParam(ObjRef, "VarBranch");
        set => SetIntParam(ObjRef, "VarBranch", value);
    }
    
    public int ZeroObjNodes
    {
        get => GetIntParam(ObjRef, "ZeroObjNodes");
        set => SetIntParam(ObjRef, "ZeroObjNodes", value);
    }

    // MIP cut parameters

    public int Cuts
    {
        get => GetIntParam(ObjRef, "Cuts");
        set => SetIntParam(ObjRef, "Cuts", value);
    }

    public int CliqueCuts
    {
        get => GetIntParam(ObjRef, "CliqueCuts");
        set => SetIntParam(ObjRef, "CliqueCuts", value);
    }

    public int CoverCuts
    {
        get => GetIntParam(ObjRef, "CoverCuts");
        set => SetIntParam(ObjRef, "CoverCuts", value);
    }

    public int FlowCoverCuts
    {
        get => GetIntParam(ObjRef, "FlowCoverCuts");
        set => SetIntParam(ObjRef, "FlowCoverCuts", value);
    }

    public int FlowPathCuts
    {
        get => GetIntParam(ObjRef, "FlowPathCuts");
        set => SetIntParam(ObjRef, "FlowPathCuts", value);
    }

    public int GUBCoverCuts
    {
        get => GetIntParam(ObjRef, "GUBCoverCuts");
        set => SetIntParam(ObjRef, "GUBCoverCuts", value);
    }

    public int ImpliedCuts
    {
        get => GetIntParam(ObjRef, "ImpliedCuts");
        set => SetIntParam(ObjRef, "ImpliedCuts", value);
    }

    public int MIPSepCuts
    {
        get => GetIntParam(ObjRef, "MIPSepCuts");
        set => SetIntParam(ObjRef, "MIPSepCuts", value);
    }

    public int MIRCuts
    {
        get => GetIntParam(ObjRef, "MIRCuts");
        set => SetIntParam(ObjRef, "MIRCuts", value);
    }

    public int ModKCuts
    {
        get => GetIntParam(ObjRef, "ModKCuts");
        set => SetIntParam(ObjRef, "ModKCuts", value);
    }

    public int NetworkCuts
    {
        get => GetIntParam(ObjRef, "NetworkCuts");
        set => SetIntParam(ObjRef, "NetworkCuts", value);
    }

    public int SubMIPCuts
    {
        get => GetIntParam(ObjRef, "SubMIPCuts");
        set => SetIntParam(ObjRef, "SubMIPCuts", value);
    }

    public int ZeroHalfCuts
    {
        get => GetIntParam(ObjRef, "ZeroHalfCuts");
        set => SetIntParam(ObjRef, "ZeroHalfCuts", value);
    }

    public int CutAggPasses
    {
        get => GetIntParam(ObjRef, "CutAggPasses");
        set => SetIntParam(ObjRef, "CutAggPasses", value);
    }

    public int CutPasses
    {
        get => GetIntParam(ObjRef, "CutPasses");
        set => SetIntParam(ObjRef, "CutPasses", value);
    }

    public int GomoryPasses
    {
        get => GetIntParam(ObjRef, "GomoryPasses");
        set => SetIntParam(ObjRef, "GomoryPasses", value);
    }

    // other parameters

    public int AggFill
    {
        get => GetIntParam(ObjRef, "AggFill");
        set => SetIntParam(ObjRef, "AggFill", value);
    }
    
    public int Aggregate
    {
        get => GetIntParam(ObjRef, "Aggregate");
        set => SetIntParam(ObjRef, "Aggregate", value);
    }
    
    public int DisplayInterval
    {
        get => GetIntParam(ObjRef, "DisplayInterval");
        set => SetIntParam(ObjRef, "DisplayInterval", value);
    }
    
    public int IISMethod
    {
        get => GetIntParam(ObjRef, "IISMethod");
        set => SetIntParam(ObjRef, "IISMethod", value);
    }
    
    public string? LogFile
    {
        get => GetStrParam(ObjRef, "LogFile");
        set => SetStrParam(ObjRef, "LogFile", value);
    }
    
    public int Method
    {
        get => GetIntParam(ObjRef, "Method");
        set => SetIntParam(ObjRef, "Method", value);
    }
    
    public int OutputFlag
    {
        get => GetIntParam(ObjRef, "OutputFlag");
        set => SetIntParam(ObjRef, "OutputFlag", value);
    }
    
    public int PreCrush
    {
        get => GetIntParam(ObjRef, "PreCrush");
        set => SetIntParam(ObjRef, "PreCrush", value);
    }
    
    public int PreDepRow
    {
        get => GetIntParam(ObjRef, "PreDepRow");
        set => SetIntParam(ObjRef, "PreDepRow", value);
    }
    
    public int PreDual
    {
        get => GetIntParam(ObjRef, "PreDual");
        set => SetIntParam(ObjRef, "PreDual", value);
    }
    
    public int PreMIQPMethod
    {
        get => GetIntParam(ObjRef, "PreMIQPMethod");
        set => SetIntParam(ObjRef, "PreMIQPMethod", value);
    }
    
    public int PrePasses
    {
        get => GetIntParam(ObjRef, "PrePasses");
        set => SetIntParam(ObjRef, "PrePasses", value);
    }
    
    public int Presolve
    {
        get => GetIntParam(ObjRef, "Presolve");
        set => SetIntParam(ObjRef, "Presolve", value);
    }
    
    public int PreSparsify
    {
        get => GetIntParam(ObjRef, "PreSparsify");
        set => SetIntParam(ObjRef, "PreSparsify", value);
    }
    
    public string? ResultFile
    {
        get => GetStrParam(ObjRef, "ResultFile");
        set => SetStrParam(ObjRef, "ResultFile", value);
    }
    
    public int Threads
    {
        get => GetIntParam(ObjRef, "Threads");
        set => SetIntParam(ObjRef, "Threads", value);
    }

    // static methods

    public static void GetVersion(out int major, out int minor, out int technical)
    {
        NativeMethods.GRBversion(out major, out minor, out technical);
    }

    public static (int major, int minor, int technical) GetVersion()
    {
        NativeMethods.GRBversion(out var major, out var minor, out var technical);
        return (major, minor, technical);
    }
}
