
//  
//  GurobiException.cs
//  gurobi-sharp
//
//  Copyright 2009-2024 Markus Uhr <uhrmar@gmail.com>.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in
//        the documentation and/or other materials provided with the
//        distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

using System;
using System.Collections.Generic;

namespace Grb;

public class GurobiException : ApplicationException
{
    private static readonly Dictionary<int,(string name,string desc)> errors = new()
    {
        { 10001, ("Out of memory", "Available memory was exhausted") },
        { 10002, ("Null argument", "NULL input value provided for a required argument") },
        { 10003, ("Invalid argument", "An invalid value was provided for a routine argument") },
        { 10004, ("Unknown attribute", "Tried to query or set an unknown attribute") },
        { 10005, ("Data not available", "Attempted to query or set an attribute that could not be accessed at that time") },
        { 10006, ("Index out of range", "Tried to query or set an attribute, but one or more of the provided indices (e.g., constraint index, variable index) was outside the range of valid values") },
        { 10007, ("Unknown parameter", "Tried to query or set an unknown parameter") },
        { 10008, ("Value out of range", "Tried to set a parameter to a value that is outside the parameter's valid range") },
        { 10009, ("No license", "Failed to obtain a valid license") },
        { 10010, ("Size limit exceeded", "Attempted to solve a model that is larger than the limit for a demo license") },
        { 10011, ("Callback", "Problem in callback") },
        { 10012, ("File read", "Failed to read the requested file") },
        { 10013, ("File write", "Failed to write the requested file") },
        { 10014, ("Numeric", "Numerical error during requested operation") },
        { 10015, ("IIS not infeasible", "Attempted to perform infeasibility analysis on a feasible model") },
        { 10016, ("Not for MIP", "Requested operation not valid for a MIP model") },
        { 10017, ("Optimization in progress", "Tried to query or modify a model while optimization was in progress") },
        { 10018, ("Duplicates", "Constraint, variable, or SOS contained duplicated indices") },
        { 10019, ("Nodefile", "Error in reading or writing a node file during MIP optimization") },
        { 10020, ("Q not PSD", "Q matrix in QP model is not positive semi-definite") },
        { 10021, ("QCP equality constraint", "QCP equality constraint specified (only inequalities are supported)") },
        { 10022, ("Network", "Problem communicating with the Gurobi Compute Server") },
        { 10023, ("Job rejected", "Gurobi Compute Server responded, but was unable to process the job (typically because the queuing time exceeded the user-specified timeout or because the queue has exceeded its maximum capacity)") },
        { 10024, ("Not supported", "Indicates that a Gurobi feature is not supported under your usage environment (for example, some advanced features are not supported in a Compute Server environment)") },
        { 10025, ("Exceed 2B nonzeros", "Indicates that the user has called a query routine on a model with more than 2 billion non-zero entries, and the result would exceed the maximum size that can be returned by that query routine. The solution is typically to move to the GRBX version of that query routine.") },
        { 10026, ("Invalid piecewise obj", "Piecewise-linear objectives must have certain properties (as described in the documentation for the various setPWLObj methods). This error indicates that one of those properties was violated.") },
        { 10027, ("Updatemode change", "The UpdateMode parameter can not be modified once a model has been created.") },
        { 10028, ("Cloud", "Problems launching a Gurobi Instant Cloud job.") },
        { 10029, ("Model modification", "Indicates that the user has modified the model in such a way that the model became invalid. For example, this happens when a general constraint exists in the model and the user deletes the resultant variable of this constraint. In such a case, the general constraint does not have any meaningful interpretation anymore. The solution is to also delete the general constraint when a resultant variable is deleted.") },
        { 10030, ("CS worker", "When you are running on a Compute Server, this error indicates that there was a problem with the worker process on the server.") },
        { 10031, ("Tune model types", "Indicates that tuning was invoked on a set of models, but the models were of different types (e.g., one an LP, another a MIP).") },
        { 10032, ("Security", "Indicates that an authentication step failed or that an operation was attempted for which the current credentials do not warrant permission.") },
        { 20001, ("Not in model", "Tried to use a constraint or variable that is not in the model, either because it was removed or because it has not yet been added") },
        { 20002, ("Failed to create model", "Failed to create the requested model") },
        { 20003, ("Internal", "Internal Gurobi error") },
    };

    private readonly int errno;

    public GurobiException(int errno, string? message) : base(CreateMessage(errno, message))
    {
        this.errno = errno;
    }

    private static string CreateMessage(int errno, string? message)
    {
        if (String.IsNullOrEmpty(message)) {
            if (errors.TryGetValue(errno, out var t)) {
                message = t.name;
            }
        }
        return !String.IsNullOrEmpty(message) ? $"Gurobi error {errno}: {message}" : $"Gurobi error {errno}";
    }

    public int ErrNo
    {
        get { return this.errno; }
    }

    public string? Error
    {
        get {
            if (errors.TryGetValue(this.errno, out var t)) {
                return t.name;
            }
            return null;
        }
    }

    public string? Description
    {
        get {
            if (errors.TryGetValue(this.errno, out var t)) {
                return t.desc;
            }
            return null;
        }
    }
}
