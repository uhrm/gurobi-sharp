
//  
//  Model.cs
//  gurobi-sharp
//
//  Copyright 2009-2024 Markus Uhr <uhrmar@gmail.com>.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in
//        the documentation and/or other materials provided with the
//        distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;

namespace Grb;

public enum AttrDatatype
{
    Char   = 0,
    Int    = 1,
    Double = 2,
    String = 3
}

public enum AttrType
{
    ModelAttribute = 0,
    VariableAttribute = 1,
    LinearConstraintAttribute =2,
    SosConstraintAttribute = 3,
    QuadraticConstraintAttribute = 4,
    GeneralConstraintAttribute = 5
}

internal enum CbWhere
{
    Polling  = 0,
    Presolve = 1,
    Simplex  = 2,
    Mip      = 3,
    MipSol   = 4,
    MipNode  = 5,
    Message  = 6,
    Barrier  = 7
}

internal enum CbWhat
{
    // Presolve
    PreColDel =  1000,
    PreRowDel =  1001,
    PreSenChg =  1002,
    PreBndChg =  1003,
    PreCoeChg =  1004,
    // Simplex
    SpxItrCnt =  2000,
    SpxObjVal =  2001,
    SpxPrimInf = 2002,
    SpxDualInf = 2003,
    SpxIsPert =  2004,
    // Mip
    MipObjBst =  3000,
    MipObjBnd =  3001,
    MipNodCnt =  3002,
    MipSolCnt =  3003,
    MipCutCnt =  3004,
    MipNodLft =  3005,
    MipItrCnt =  3006,
    // MipSol
    MipSolSol =    4001,
    MipSolObj =    4002,
    MipSolObjBst = 4003,
    MipSolObjBnd = 4004,
    MipSolNodCnt = 4005,
    MipSolSolCnt = 4006,
    // MipNode
    MipNodeStatus = 5001,
    MipNodeRel =    5002,
    MipNodeObjBst = 5003,
    MipNodeObjBnd = 5004,
    MipNodeNodCnt = 5005,
    MipNodeSolCnt = 5006,
    MipNodeBrVar =  5007,
    // Message
    MsgString =  6001,
    Runtime =    6002,
    // Barrier
    BarrierItrCnt =  7001,
    BarrierPrimObj = 7002,
    BarrierDualObj = 7003,
    BarrierPrimInf = 7004,
    BarrierDualInf = 7005,
    BarrierCompl =   7006
}

internal delegate int GrbCallback(IntPtr model, IntPtr data, int where, IntPtr usrdata);

internal sealed class InternalCallback<T>
{
    private readonly PollingCallback<T>? polling;
    private readonly PresolveCallback<T>? presolve;
    private readonly SimplexCallback<T>? simplex;
    private readonly MipCallback<T>? mip;
    private readonly MipSolCallback<T>? mipsol;
    private readonly MipNodeCallback<T>? mipnode;
    private readonly MessageCallback<T>? message;
    private readonly BarrierCallback<T>? barrier;
    private readonly T? userdata;

    internal InternalCallback(
        PollingCallback<T>? polling,
        PresolveCallback<T>? presolve,
        SimplexCallback<T>? simplex,
        MipCallback<T>? mip,
        MipSolCallback<T>? mipsol,
        MipNodeCallback<T>? mipnode,
        MessageCallback<T>? message,
        BarrierCallback<T>? barrier,
        T? userdata
    ) {
        this.polling = polling;
        this.presolve = presolve;
        this.simplex = simplex;
        this.mip = mip;
        this.mipsol = mipsol;
        this.mipnode = mipnode;
        this.message = message;
        this.barrier = barrier;
        this.userdata = userdata;
    }

    internal int CbFunc(IntPtr model, IntPtr cbdata, int where, IntPtr usrdata)
    {
        try
        {
            switch ((CbWhere)where)
            {
            case CbWhere.Polling when this.polling != null:
                this.polling(new Model(model, false), this.userdata);
                break;
            case CbWhere.Presolve when this.presolve != null:
                this.presolve(new Model(model, false), new PresolveInfo(model, cbdata), this.userdata);
                break;
            case CbWhere.Simplex when this.simplex != null:
                this.simplex(new Model(model, false), new SimplexInfo(model, cbdata), this.userdata);
                break;
            case CbWhere.Mip when this.mip != null:
                this.mip(new Model(model, false), new MipInfo(model, cbdata), this.userdata);
                break;
            case CbWhere.MipSol when this.mipsol != null:
                this.mipsol(new Model(model, false), new MipSolInfo(model, cbdata), this.userdata);
                break;
            case CbWhere.MipNode when this.mipnode != null:
                this.mipnode(new Model(model, false), new MipNodeInfo(model, cbdata), this.userdata);
                break;
            case CbWhere.Message when this.message != null:
                this.message(new Model(model, false), new MessageInfo(model, cbdata), this.userdata);
                break;
            case CbWhere.Barrier when this.barrier != null:
                this.barrier(new Model(model, false), new BarrierInfo(model, cbdata), this.userdata);
                break;
            default:
                break;
            }
            return 0;
        }
        catch (GurobiException ex)
        {
            return ex.ErrNo;
        }
    }

    public double Runtime()
    {
        return 0.0;
    }
}

public abstract class CallbackInfo
{
    private readonly IntPtr model;
    private readonly IntPtr cbdata;

    internal CallbackInfo(IntPtr model, IntPtr cbdata)
    {
        this.model = model;
        this.cbdata = cbdata;
    }

    internal void GrbErr(int errno)
    {
        if (errno != 0) {
            var env = NativeMethods.GRBgetenv(this.model);
            var errmsg = NativeMethods.GRBgeterrormsg(env);
            throw new GurobiException(errno, errmsg);
        }
    }
    internal int GetInt(int where, int what)
    {
        GrbErr(NativeMethods.GRBcbget(this.cbdata, where, what, out int data));
        return data;
    }

    internal double GetDouble(int where, int what)
    {
        GrbErr(NativeMethods.GRBcbget(this.cbdata, where, what, out double data));
        return data;
    }

    internal void GetDouble(int where, int what, double[] data)
    {
        GrbErr(NativeMethods.GRBcbget(this.cbdata, where, what, data));
    }

    internal string? GetString(int where, int what)
    {
        GrbErr(NativeMethods.GRBcbget(this.cbdata, (int)CbWhere.Message, (int)CbWhat.MsgString, out IntPtr result));
        return Marshal.PtrToStringAnsi(result);
    }
}

public delegate int PollingCallback<T>(Model model, T? usrdata);

public class PresolveInfo : CallbackInfo
{
    internal PresolveInfo(IntPtr model, IntPtr cbdata) : base(model, cbdata) { }

    public int ColDel
    {
        get {
            return GetInt((int)CbWhere.Presolve, (int)CbWhat.PreColDel);
        }
    }
}

public delegate int PresolveCallback<T>(Model model, PresolveInfo info, T? usrdata);

public class SimplexInfo : CallbackInfo
{
    internal SimplexInfo(IntPtr model, IntPtr cbdata) : base(model, cbdata) { }

    public double ItrCnt {
        get { return GetDouble((int)CbWhere.Simplex, (int)CbWhat.SpxItrCnt); }
    }
}

public delegate int SimplexCallback<T>(Model model, SimplexInfo info, T? usrdata);

public class MipInfo : CallbackInfo
{
    internal MipInfo(IntPtr model, IntPtr cbdata) : base(model, cbdata) { }

    public double ObjBst
    {
        get { return GetDouble((int)CbWhere.Mip, (int)CbWhat.MipObjBst); }
    }

    public double ObjBnd
    {
        get { return GetDouble((int)CbWhere.Mip, (int)CbWhat.MipObjBnd); }
    }

    public double NodCnt
    {
        get { return GetDouble((int)CbWhere.Mip, (int)CbWhat.MipNodCnt); }
    }

    public int SolCnt
    {
        get { return GetInt((int)CbWhere.Mip, (int)CbWhat.MipSolCnt); }
    }

    public int CutCnt
    {
        get { return GetInt((int)CbWhere.Mip, (int)CbWhat.MipCutCnt); }
    }

    public double ItrCnt
    {
        get { return GetDouble((int)CbWhere.Mip, (int)CbWhat.MipItrCnt); }
    }

//        public int ItrCnt
//        {
//            get { return GetInt((int)CbWhere.Mip, (int)CbWhat.MipItrCnt); }
//        }
}

public delegate int MipCallback<T>(Model model, MipInfo info, T? usrdata);

public class MipSolInfo : CallbackInfo
{
    internal MipSolInfo(IntPtr model, IntPtr cbdata) : base(model, cbdata) { }

    public double NodCnt
    {
        get { return GetDouble((int)CbWhere.MipSol, (int)CbWhat.MipSolNodCnt); }
    }
}

public delegate int MipSolCallback<T>(Model model, MipSolInfo info, T? usrdata);

public class MipNodeInfo : CallbackInfo
{
    internal MipNodeInfo(IntPtr model, IntPtr cbdata) : base(model, cbdata) { }

    public ModelStatus Status
    {
        get { return (ModelStatus)GetInt((int)CbWhere.MipNode, (int)CbWhat.MipNodeStatus); }
    }

    public double ObjBst
    {
        get { return GetDouble((int)CbWhere.MipNode, (int)CbWhat.MipNodeObjBst); }
    }

    public double ObjBnd
    {
        get { return GetDouble((int)CbWhere.MipNode, (int)CbWhat.MipNodeObjBnd); }
    }

    public double NodCnt
    {
        get { return GetDouble((int)CbWhere.MipNode, (int)CbWhat.MipNodeNodCnt); }
    }

    public int SolCnt
    {
        get { return GetInt((int)CbWhere.MipNode, (int)CbWhat.MipNodeSolCnt); }
    }

    public void Rel(double[] x)
    {
        GetDouble((int)CbWhere.MipNode, (int)CbWhat.MipNodeRel, x);
    }
}

public delegate int MipNodeCallback<T>(Model model, MipNodeInfo info, T? usrdata);

public class MessageInfo : CallbackInfo
{
    internal MessageInfo(IntPtr model, IntPtr cbdata) : base(model, cbdata) { }

    public string? MsgString
    {
        get { return GetString((int)CbWhere.Message, (int)CbWhat.MsgString); }
    }
}

public delegate int MessageCallback<T>(Model model, MessageInfo info, T? usrdata);

public class BarrierInfo : CallbackInfo
{
    internal BarrierInfo(IntPtr model, IntPtr cbdata) : base(model, cbdata) { }

    public int ItrCnt
    {
        get { return GetInt((int)CbWhere.Barrier, (int)CbWhat.BarrierItrCnt); }
    }
}

public delegate int BarrierCallback<T>(Model model, BarrierInfo info, T? usrdata);


public sealed class Model : GurobiObject
{
    // model construction

    private static IntPtr NewModel(Environment env, string name, int numvars, double[]? obj, double[]? lb, double[]? ub, VariableType[]? vtype, string?[]? varnames)
    {
        if (obj != null && obj.Length != numvars)
            throw new ArgumentException($"Invalid length of \"{nameof(obj)}\" array (expected {numvars}, found {obj.Length})");
        if (lb != null && lb.Length != numvars)
            throw new ArgumentException($"Invalid length of \"{nameof(lb)}\" array (expected {numvars}, found {lb.Length})");
        if (ub != null && ub.Length != numvars)
            throw new ArgumentException($"Invalid length of \"{nameof(ub)}\" array (expected {numvars}, found {ub.Length})");
        if (vtype != null && vtype.Length != numvars)
            throw new ArgumentException($"Invalid length of \"{nameof(vtype)}\" array (expected {numvars}, found {vtype.Length})");
        if (varnames != null && varnames.Length != numvars)
            throw new ArgumentException($"Invalid length of \"{nameof(varnames)}\" array (expected {numvars}, found {varnames.Length})");
        var vt = (byte[]?)null;
        if (vtype != null) {
            vt = new byte[numvars];
            for (var i=0; i<numvars; i++) {
                vt[i] = (byte)vtype[i];
            }
        } 
        var errno = NativeMethods.GRBnewmodel(env.ObjRef, out var ptr, name, numvars, obj, lb, ub, vt, varnames);
        GrbErr(env.ObjRef, errno);
        return ptr;
    }

    private static IntPtr CopyModel(Model model)
    {
        var ptr = NativeMethods.GRBcopymodel(model.ObjRef);
        if (ptr == IntPtr.Zero) {
            throw new GurobiException(-1, "Error copying model.");
        }
        return ptr;
    }

    internal Model(IntPtr ptr, bool owner) : base(ptr, owner) { }

    public Model(Environment env, string name)
        : this(env, name, 0, null, null, null, null, null) { }

    public Model(Environment env, string name, int numvars, double[]? obj, double[]? lb, double[]? ub, VariableType[]? vtype, string?[]? varnames)
        : base(NewModel(env, name, numvars, obj, lb, ub, vtype, varnames), true) { }

    public Model(Model model)
        : base(CopyModel(model), true) { }

    ~Model()
    {
        Dispose(false);
    }

    // IDisposable stuff

    internal override void FreeRef()
    {
        NativeMethods.GRBfreemodel(ObjRef);
    }

    // error handling

    private void GrbErr(int errno)
    {
        if (errno != 0) {
            var env = NativeMethods.GRBgetenv(this.ObjRef);
            var errmsg = NativeMethods.GRBgeterrormsg(env);
            throw new GurobiException(errno, errmsg);
        }
    }

    private static void GrbErr(IntPtr env, int errno)
    {
        if (errno != 0) {
            var errmsg = NativeMethods.GRBgeterrormsg(env);
            throw new GurobiException(errno, errmsg);
        }
    }

    // model modification methods

    public void AddVar(double obj, double lb, double ub, VariableType vtype, string? name = null)
    {
        GrbErr(NativeMethods.GRBaddvar(ObjRef, 0, MemoryMarshal.GetReference<int>(null), MemoryMarshal.GetReference<double>(null), obj, lb, ub, vtype, name));
    }

    public void AddVar(ReadOnlySpan<int> idx, ReadOnlySpan<double> val, double obj, double lb, double ub, VariableType vtype, string? name = null)
    {
        var nnz = idx.Length;
        if (val.Length != nnz)
            throw new ArgumentException($"Arrays \"{nameof(idx)}\" and \"{nameof(val)}\" must have same length, but have length {idx.Length} and {val.Length}.");
        GrbErr(NativeMethods.GRBaddvar(ObjRef, nnz, MemoryMarshal.GetReference<int>(idx), MemoryMarshal.GetReference<double>(val), obj, lb, ub, vtype, name));
    }

    public void AddVar(int nnz, int[] idx, double[] val, double obj, double lb, double ub, VariableType vtype, string? name = null)
    {
        if (idx.Length < nnz)
            throw new ArgumentException($"Array \"{nameof(idx)}\" is too short (expected length >= {nnz}, but found {idx.Length}).", nameof(idx));
        if (val.Length < nnz)
            throw new ArgumentException($"Array \"{nameof(val)}\" is too short (expected length >= {nnz}, but found {val.Length}).", nameof(val));
        GrbErr(NativeMethods.GRBaddvar(ObjRef, nnz, MemoryMarshal.GetReference<int>(idx), MemoryMarshal.GetReference<double>(val), obj, lb, ub, vtype, name));
    }

    public void AddVars(double[] obj, double[] lb, double[] ub, VariableType[] vtype, string[]? name = null)
    {
        var nvars = obj.Length;
        if (lb.Length != nvars)
            throw new ArgumentException($"Arrays \"{nameof(obj)}\" and \"{nameof(lb)}\" must have same length, but have length {obj.Length} and {lb.Length}.");
        if (ub.Length != nvars)
            throw new ArgumentException($"Arrays \"{nameof(obj)}\" and \"{nameof(ub)}\" must have same length, but have length {obj.Length} and {ub.Length}.");
        if (vtype.Length != nvars)
            throw new ArgumentException($"Arrays \"{nameof(obj)}\" and \"{nameof(vtype)}\" must have same length, but have length {obj.Length} and {vtype.Length}.");
        if (name != null && name.Length != nvars)
            throw new ArgumentException($"Arrays \"{nameof(obj)}\" and \"{nameof(name)}\" must have same length, but have length {obj.Length} and {name.Length}.");
        GrbErr(NativeMethods.GRBaddvars(ObjRef, nvars, 0, null, null, null, obj, lb, ub, vtype, name));
    }

    public void AddVars(int[] ofs, int[] idx, double[] val, double[] obj, double[] lb, double[] ub, VariableType[] vtype, string[]? name = null)
    {
        var nnz = idx.Length;
        if (val.Length != nnz)
            throw new ArgumentException($"Arrays \"{nameof(idx)}\" and \"{nameof(val)}\" must have same length, but have length {idx.Length} and {val.Length}.");
        var nvars = ofs.Length - 1;
        if (obj.Length != nvars)
            throw new ArgumentException($"Invalid length of array \"{nameof(obj)}\": expected {nvars}, found {obj.Length}.");
        if (lb.Length != nvars)
            throw new ArgumentException($"Invalid length of array \"{nameof(lb)}\": expected {nvars}, found {lb.Length}.");
        if (ub.Length != nvars)
            throw new ArgumentException($"Invalid length of array \"{nameof(ub)}\": expected {nvars}, found {ub.Length}.");
        if (vtype.Length != nvars)
            throw new ArgumentException($"Invalid length of array \"{nameof(vtype)}\": expected {nvars}, found {vtype.Length}.");
        if (name != null && name.Length != nvars)
            throw new ArgumentException($"Invalid length of array \"{nameof(name)}\": expected {nvars}, found {name.Length}.");
        GrbErr(NativeMethods.GRBaddvars(ObjRef, nvars, nnz, ofs, idx, val, obj, lb, ub, vtype, name));
    }

    public void AddConstr(ConstrSense sense, double rhs, string? name = null)
    {
        GrbErr(NativeMethods.GRBaddconstr(ObjRef, 0, MemoryMarshal.GetReference<int>(null), MemoryMarshal.GetReference<double>(null), sense, rhs, name));
    }

    public void AddConstr(ReadOnlySpan<int> idx, ReadOnlySpan<double> val, ConstrSense sense, double rhs, string? name = null)
    {
        var nnz = idx.Length;
        if (val.Length != nnz)
            throw new ArgumentException($"Arrays \"{nameof(idx)}\" and \"{nameof(val)}\" must have same length, but have length {idx.Length} and {val.Length}.");
        GrbErr(NativeMethods.GRBaddconstr(ObjRef, nnz, MemoryMarshal.GetReference<int>(idx), MemoryMarshal.GetReference<double>(val), sense, rhs, name));
    }
    
    public void AddConstr(int nnz, int[] idx, double[] val, ConstrSense sense, double rhs, string? name = null)
    {
        if (idx.Length < nnz)
            throw new ArgumentException($"Array \"{nameof(idx)}\" is too short (expected length >= {nnz}, but found {idx.Length}).", nameof(idx));
        if (val.Length < nnz)
            throw new ArgumentException($"Array \"{nameof(val)}\" is too short (expected length >= {nnz}, but found {val.Length}).", nameof(val));
        GrbErr(NativeMethods.GRBaddconstr(ObjRef, nnz, MemoryMarshal.GetReference<int>(idx), MemoryMarshal.GetReference<double>(val), sense, rhs, name));
    }

    public void AddConstr(IEnumerable<(int,double)> data, ConstrSense sense, double rhs, string? name = null)
    {
        var nnz = data.Count();
        var idx = new int[nnz];
        var val = new double[nnz];
        var k = 0;
        foreach (var (i, v) in data) {
            idx[k] = i;
            val[k] = v;
            k++;
        }
        GrbErr(NativeMethods.GRBaddconstr(ObjRef, nnz, MemoryMarshal.GetReference<int>(idx), MemoryMarshal.GetReference<double>(val), sense, rhs, name));
    }

    public void AddConstrs(ConstrSense[] sense, double[] rhs, string[]? name = null)
    {
        var nrows = sense.Length;
        if (rhs.Length != nrows)
            throw new ArgumentException($"Arrays \"{nameof(sense)}\" and \"{nameof(rhs)}\" must have same length, but have length {sense.Length} and {rhs.Length}.");
        if (name != null && name.Length != nrows)
            throw new ArgumentException($"Arrays \"{nameof(sense)}\" and \"{nameof(name)}\" must have same length, but have length {sense.Length} and {name.Length}.");
        GrbErr(NativeMethods.GRBaddconstrs(ObjRef, nrows, 0, null, null, null, sense, rhs, name));
    }

    public void AddConstrs(int[] ofs, int[] idx, double[] val, ConstrSense[] sense, double[] rhs, string[]? name = null)
    {
        var nnz = idx.Length;
        if (val.Length != nnz)
            throw new ArgumentException($"Arrays \"{nameof(idx)}\" and \"{nameof(val)}\" must have same length, but have length {idx.Length} and {val.Length}.");
        var nrows = ofs.Length - 1;
        if (sense.Length != nrows)
            throw new ArgumentException($"Invalid length of array \"{nameof(sense)}\": expected {nrows}, found {sense.Length}.");
        if (rhs.Length != nrows)
            throw new ArgumentException($"Invalid length of array \"{nameof(rhs)}\": expected {nrows}, found {rhs.Length}.");
        if (name != null && name.Length != nrows)
            throw new ArgumentException($"Invalid length of array \"{nameof(sense)}\": expected {nrows}, found {name.Length}.");
        GrbErr(NativeMethods.GRBaddconstrs(ObjRef, nrows, nnz, ofs, idx, val, sense, rhs, name));
    }

    public void AddRangeConstr(ReadOnlySpan<int> idx, ReadOnlySpan<double> val, double lower, double upper, string? name = null)
    {
        var nnz = idx.Length;
        if (val.Length != nnz)
            throw new ArgumentException($"Arrays \"{nameof(idx)}\" and \"{nameof(val)}\" must have same length, but have length {idx.Length} and {val.Length}.");
        GrbErr(NativeMethods.GRBaddrangeconstr(ObjRef, nnz, MemoryMarshal.GetReference<int>(idx), MemoryMarshal.GetReference<double>(val), lower, upper, name));
    }

    public void AddRangeConstr(int nnz, int[] idx, double[] val, double lower, double upper, string? name = null)
    {
        if (idx.Length < nnz)
            throw new ArgumentException($"Array \"{nameof(idx)}\" is too short (expected length >= {nnz}, but found {idx.Length}).", nameof(idx));
        if (val.Length < nnz)
            throw new ArgumentException($"Array \"{nameof(val)}\" is too short (expected length >= {nnz}, but found {val.Length}).", nameof(val));
        GrbErr(NativeMethods.GRBaddrangeconstr(ObjRef, nnz, MemoryMarshal.GetReference<int>(idx), MemoryMarshal.GetReference<double>(val), lower, upper, name));
    }

    public void AddRangeConstr(IEnumerable<(int,double)> data, double lower, double upper, string? name = null)
    {
        var nnz = data.Count();
        var idx = new int[nnz];
        var val = new double[nnz];
        var k = 0;
        foreach (var (i, v) in data) {
            idx[k] = i;
            val[k] = v;
            k++;
        }
        GrbErr(NativeMethods.GRBaddrangeconstr(ObjRef, nnz, MemoryMarshal.GetReference<int>(idx), MemoryMarshal.GetReference<double>(val), lower, upper, name));
    }

    public void AddSos(SosType type, ReadOnlySpan<int> ind, ReadOnlySpan<double> weight)
    {
        var nvars = ind.Length;
        if (weight.Length != nvars)
            throw new ArgumentException($"Invalid length of array \"{nameof(weight)}\": expected {nvars}, found {weight.Length}.");
        GrbErr(NativeMethods.GRBaddsos(ObjRef, 1, nvars, type, 0, MemoryMarshal.GetReference<int>(ind), MemoryMarshal.GetReference<double>(weight)));
    }

    public void AddSos(ReadOnlySpan<SosType> types, ReadOnlySpan<int> beg, ReadOnlySpan<int> ind, ReadOnlySpan<double> weight)
    {
        var nsos = types.Length;
        if (beg.Length != nsos)
            throw new ArgumentException($"Invalid length of array \"{nameof(beg)}\": expected {nsos}, found {beg.Length}.");
        var nvars = ind.Length;
        if (weight.Length != nvars)
            throw new ArgumentException($"Invalid length of array \"{nameof(weight)}\": expected {nvars}, found {weight.Length}.");
        GrbErr(NativeMethods.GRBaddsos(ObjRef, nsos, nvars, MemoryMarshal.GetReference<SosType>(types), MemoryMarshal.GetReference<int>(beg), MemoryMarshal.GetReference<int>(ind), MemoryMarshal.GetReference<double>(weight)));
    }

    public void DelVar(int idx)
    {
        GrbErr(NativeMethods.GRBdelvars(ObjRef, 1, idx));
    }
    
    public void DelVars(ReadOnlySpan<int> idx)
    {
        GrbErr(NativeMethods.GRBdelvars(ObjRef, idx.Length, MemoryMarshal.GetReference<int>(idx)));
    }

    public void DelConstr(int idx)
    {
        GrbErr(NativeMethods.GRBdelconstrs(ObjRef, 1, idx));
    }

    public void DelConstrs(ReadOnlySpan<int> idx)
    {
        GrbErr(NativeMethods.GRBdelconstrs(ObjRef, idx.Length, MemoryMarshal.GetReference<int>(idx)));
    }

    public void DelSos(int ind)
    {
        GrbErr(NativeMethods.GRBdelsos(ObjRef, 1, ind));
    }

    public void DelSos(ReadOnlySpan<int> ind)
    {
        GrbErr(NativeMethods.GRBdelsos(ObjRef, ind.Length, MemoryMarshal.GetReference<int>(ind)));
    }
    
    public void Update()
    {
        GrbErr(NativeMethods.GRBupdatemodel(ObjRef));
    }
    
    public void Optimize()
    {
        GrbErr(NativeMethods.GRBoptimize(ObjRef));
    }

    public void ComputeIIS()
    {
        GrbErr(NativeMethods.GRBcomputeIIS(ObjRef));
    }
    
    // model query methods
    
    public double GetCoeff(int i, int j)
    {
        GrbErr(NativeMethods.GRBgetcoeff(ObjRef, i, j, out var coef));
        return coef;
    }

    // callback infrastructure

    private GrbCallback? callbackfunc;

    public void RegisterCallback<T>(
        PollingCallback<T>? polling = null,
        PresolveCallback<T>? presolve = null,
        SimplexCallback<T>? simplex = null,
        MipCallback<T>? mip = null,
        MipSolCallback<T>? mipsol = null,
        MipNodeCallback<T>? mipnode = null,
        MessageCallback<T>? message = null,
        BarrierCallback<T>? barrier = null,
        T? userdata = default
    ) {
        if (polling != null || presolve != null || simplex != null || mip != null ||
            mipsol != null || mipnode != null || message != null || barrier != null) {
            var cb = new InternalCallback<T>(polling, presolve, simplex, mip, mipsol, mipnode, message, barrier, userdata);
            this.callbackfunc = new GrbCallback(cb.CbFunc);
            GrbErr(NativeMethods.GRBsetcallbackfunc(ObjRef, Marshal.GetFunctionPointerForDelegate(callbackfunc), IntPtr.Zero));
        }
        else {
            this.callbackfunc = null;
            GrbErr(NativeMethods.GRBsetcallbackfunc(ObjRef, IntPtr.Zero, IntPtr.Zero));
        }
    }

    // model I/O

    public void Read(string filename)
    {
        GrbErr(NativeMethods.GRBread(ObjRef, filename));
    }

    public void Write(string filename)
    {
        GrbErr(NativeMethods.GRBwrite(ObjRef, filename));
    }

    public static Model ReadModel(Environment env, string filename)
    {
        GrbErr(env.ObjRef, NativeMethods.GRBreadmodel(env.ObjRef, filename, out var ptr));
        return new Model(ptr, true);
    }
    
    // model properties

    public Environment Environment
    {
        get
        {
            var ptr = NativeMethods.GRBgetenv(ObjRef);
            return new Environment(ptr);
        }
    }

    //
    // attributes
    //
    
    // "typed indexers" for indexed attributes

    public class RoVecIntAttr
    {
        private readonly Model model;
        private readonly string attr;
        internal RoVecIntAttr(Model m, string a)
        {
            model = m;
            attr = a;
        }
        public int this[int i]
        {
            get
            {
                model.GrbErr(NativeMethods.GRBgetintattrelement(model.ObjRef, attr, i, out var p));
                return p;
            }
        }
    }
    
    public class RwVecIntAttr
    {
        private readonly Model model;
        private readonly string attr;
        internal RwVecIntAttr(Model m, string a)
        {
            model = m;
            attr = a;
        }
        public int this[int i]
        {
            get
            {
                model.GrbErr(NativeMethods.GRBgetintattrelement(model.ObjRef, attr, i, out var p));
                return p;
            }
            set
            {
                model.GrbErr(NativeMethods.GRBsetintattrelement(model.ObjRef, attr, i, value));
            }
        }
    }
    
    public class RwVecVtAttr
    {
        private readonly Model model;
        private readonly string attr;
        internal RwVecVtAttr(Model m, string a)
        {
            model = m;
            attr = a;
        }
        public VariableType this[int i]
        {
            get
            {
                model.GrbErr(NativeMethods.GRBgetcharattrelement(model.ObjRef, attr, i, out var p));
                return (VariableType)p;
            }
            set
            {
                model.GrbErr(NativeMethods.GRBsetcharattrelement(model.ObjRef, attr, i, (byte)value));
            }
        }
    }
    
    public class RwVecVbAttr
    {
        private readonly Model model;
        private readonly string attr;
        internal RwVecVbAttr(Model m, string a)
        {
            model = m;
            attr = a;
        }
        public VariableStatus this[int i]
        {
            get
            {
                model.GrbErr(NativeMethods.GRBgetintattrelement(model.ObjRef, attr, i, out var p));
                return (VariableStatus)p;
            }
            set
            {
                model.GrbErr(NativeMethods.GRBsetintattrelement(model.ObjRef, attr, i, (int)value));
            }
        }
    }
    
    public class RwVecCsAttr
    {
        private readonly Model model;
        private readonly string attr;
        internal RwVecCsAttr(Model m, string a)
        {
            model = m;
            attr = a;
        }
        public ConstrSense this[int i]
        {
            get
            {
                model.GrbErr(NativeMethods.GRBgetcharattrelement(model.ObjRef, attr, i, out var c));
                return (ConstrSense)c;
            }
            set
            {
                model.GrbErr(NativeMethods.GRBsetcharattrelement(model.ObjRef, attr, i, (byte)value));
            }
        }
    }
    
    public class RwVecCbAttr
    {
        private readonly Model model;
        private readonly string attr;
        internal RwVecCbAttr(Model m, string a)
        {
            model = m;
            attr = a;
        }
        public ConstraintStatus this[int i]
        {
            get
            {
                model.GrbErr(NativeMethods.GRBgetintattrelement(model.ObjRef, attr, i, out var p));
                return (ConstraintStatus)p;
            }
            set
            {
                model.GrbErr(NativeMethods.GRBsetintattrelement(model.ObjRef, attr, i, (int)value));
            }
        }
    }
    
    public class RoVecDblAttr
    {
        private readonly Model model;
        private readonly string attr;
        internal RoVecDblAttr(Model m, string a)
        {
            model = m;
            attr = a;
        }
        public double this[int i]
        {
            get
            {
                model.GrbErr(NativeMethods.GRBgetdblattrelement(model.ObjRef, attr, i, out var p));
                return p;
            }
        }
    }
    
    public class RwVecDblAttr
    {
        private readonly Model model;
        private readonly string attr;
        internal RwVecDblAttr(Model m, string a)
        {
            model = m;
            attr = a;
        }
        public double this[int i]
        {
            get
            {
                model.GrbErr(NativeMethods.GRBgetdblattrelement(model.ObjRef, attr, i, out var p));
                return p;
            }
            set
            {
                model.GrbErr(NativeMethods.GRBsetdblattrelement(model.ObjRef, attr, i, value));
            }
        }
    }
    
    public class RoVecStrAttr
    {
        private readonly Model model;
        private readonly string attr;
        internal RoVecStrAttr(Model m, string a)
        {
            model = m;
            attr = a;
        }
        public string? this[int i]
        {
            get
            {
                model.GrbErr(NativeMethods.GRBgetstrattrelement(model.ObjRef, attr, i, out var p));
                return p;
            }
        }
    }
    
    public class RwVecStrAttr
    {
        private readonly Model model;
        private readonly string attr;
        internal RwVecStrAttr(Model m, string a)
        {
            model = m;
            attr = a;
        }
        public string? this[int i]
        {
            get
            {
                model.GrbErr(NativeMethods.GRBgetstrattrelement(model.ObjRef, attr, i, out var p));
                return p;
            }
            set
            {
                model.GrbErr(NativeMethods.GRBsetstrattrelement(model.ObjRef, attr, i, value));
            }
        }
    }
    
    public class RoVecBoolAttr
    {
        private readonly Model model;
        private readonly string attr;
        internal RoVecBoolAttr(Model m, string a)
        {
            model = m;
            attr = a;
        }
        public bool this[int i]
        {
            get
            {
                model.GrbErr(NativeMethods.GRBgetintattrelement(model.ObjRef, attr, i, out var p));
                return p != 0;
            }
        }
    }

    private int GetIntAttr(nint model, string name)
    {
        GrbErr(NativeMethods.GRBgetintattr(model, name, out var p));
        return p;
    }

    private double GetDblAttr(nint model, string name)
    {
        GrbErr(NativeMethods.GRBgetdblattr(model, name, out var p));
        return p;
    }

    private string? GetStrAttr(nint model, string name)
    {
        GrbErr(NativeMethods.GRBgetstrattr(model, name, out var p));
        return p;
    }
    

    // model attributes
    
    public int NumConstrs => GetIntAttr(ObjRef, "NumConstrs");
    
    public int NumVars => GetIntAttr(ObjRef, "NumVars");
    
    public int NumSos => GetIntAttr(ObjRef, "NumSOS");

    public int NumQConstrs => GetIntAttr(ObjRef, "NumQConstrs");

    public int NumGenConstrs => GetIntAttr(ObjRef, "NumGenConstrs");
    
    public int NumNzs => GetIntAttr(ObjRef, "NumNZs");

    public int DNumNZs => GetIntAttr(ObjRef, "DNumNZs");
    
    public int NumQnzs => GetIntAttr(ObjRef, "NumQNZs");

    public int NumQCNZs => GetIntAttr(ObjRef, "NumQCNZs");
    
    public int NumIntVars => GetIntAttr(ObjRef, "NumIntVars");
    
    public int NumBinVars => GetIntAttr(ObjRef, "NumBinVars");

    public int NumPWLObjVars => GetIntAttr(ObjRef, "NumPWLObjVars");
    
    public string? ModelName
    {
        get => GetStrAttr(ObjRef, "ModelName");
        set { GrbErr(NativeMethods.GRBsetstrattr(ObjRef, "ModelName", value)); }
    }
    
    public ModelSense ModelSense
    {
        get => (ModelSense)GetIntAttr(ObjRef, "ModelSense");
        set { GrbErr(NativeMethods.GRBsetintattr(ObjRef, "ModelSense", (int)value)); }
    }
    
    public double ObjCon
    {
        get=> GetDblAttr(ObjRef, "ObjCon");
        set { GrbErr(NativeMethods.GRBsetdblattr(ObjRef, "ObjCon", value)); }
    }
    
    public bool IsMip => GetIntAttr(ObjRef, "IsMIP") != 0;
    
    public bool IsQp => GetIntAttr(ObjRef, "IsQP") != 0;

    public bool IsQcp => GetIntAttr(ObjRef, "IsQCP") != 0;

    public bool IsMultiObj => GetIntAttr(ObjRef, "IsMultiObj") != 0;

    public int LicenseExpiration => GetIntAttr(ObjRef, "LicenseExpiration");

    public int NumTagged => GetIntAttr(ObjRef, "NumTagged");

    public int Fingerprint => GetIntAttr(ObjRef, "Fingerprint");

    // model solution attributes
    
    public double ObjVal => GetDblAttr(ObjRef, "ObjVal");

    public double ObjBound => GetDblAttr(ObjRef, "ObjBound");

    public double ObjBoundC => GetDblAttr(ObjRef, "ObjBoundC");
    
    public double PoolObjVal => GetDblAttr(ObjRef, "PoolObjVal");

    public double PoolObjBound => GetDblAttr(ObjRef, "PoolObjBound");
    
    public double Runtime => GetDblAttr(ObjRef, "Runtime");
    
    public double Work => GetDblAttr(ObjRef, "Work");
    
    public ModelStatus Status => (ModelStatus)GetIntAttr(ObjRef, "Status");
    
    public double MipGap => GetDblAttr(ObjRef, "MIPGap");

    public int SolCount => GetIntAttr(ObjRef, "SolCount");
    
    public double IterCount => GetDblAttr(ObjRef, "IterCount");
    
    public int BarIterCount => GetIntAttr(ObjRef, "BarIterCount");
    
    public double NodeCount => GetDblAttr(ObjRef, "NodeCount");
    
    public double OpenNodeCount => GetDblAttr(ObjRef, "OpenNodeCount");

    public int HasDualNorm => GetIntAttr(ObjRef, "HasDualNorm");

    public int ConcurrentWinMethod => GetIntAttr(ObjRef, "ConcurrentWinMethod");

    // model statistics attributes

    public double MaxCoeff => GetDblAttr(ObjRef, "MaxCoeff");
    
    public double MinCoeff => GetDblAttr(ObjRef, "MinCoeff");
    
    public double MaxBound => GetDblAttr(ObjRef, "MaxBound");
    
    public double MinBound => GetDblAttr(ObjRef, "MinBound");
    
    public double MaxObjCoeff => GetDblAttr(ObjRef, "MaxObjCoeff");
    
    public double MinObjCoeff => GetDblAttr(ObjRef, "MinObjCoeff");
    
    public double MaxRhs => GetDblAttr(ObjRef, "MaxRHS");
    
    public double MinRhs => GetDblAttr(ObjRef, "MinRHS");

    public double MaxQCCoeff => GetDblAttr(ObjRef, "MaxQCCoeff");

    public double MinQCCoeff => GetDblAttr(ObjRef, "MinQCCoeff");

    public double MaxQObjCoeff => GetDblAttr(ObjRef, "MaxQObjCoeff");

    public double MinQObjCoeff => GetDblAttr(ObjRef, "MinQObjCoeff");

    public double MaxQCLCoeff => GetDblAttr(ObjRef, "MaxQCLCoeff");

    public double MinQCLCoeff => GetDblAttr(ObjRef, "MinQCLCoeff");

    public double MaxQCRHS => GetDblAttr(ObjRef, "MaxQCRHS");

    public double MinQCRHS => GetDblAttr(ObjRef, "MinQCRHS");

    // variable attributes
    
    private RwVecDblAttr? _Lb;
    public RwVecDblAttr Lb => this._Lb ??= new RwVecDblAttr(this, "LB");
    
    private RwVecDblAttr? _Ub;
    public RwVecDblAttr Ub => this._Ub ??= new RwVecDblAttr(this, "UB");
    
    private RwVecDblAttr? _Obj;
    public RwVecDblAttr Obj => this._Obj ??= new RwVecDblAttr(this, "Obj");
    
    private RwVecVtAttr? _VType;
    public RwVecVtAttr VType => this._VType ??= new RwVecVtAttr(this, "VType");
    
    private RwVecStrAttr? _VarName;
    public RwVecStrAttr VarName => this._VarName ??= new RwVecStrAttr(this, "VarName");
    
    private RoVecDblAttr? _Start;
    public RoVecDblAttr Start => this._Start ??= new RoVecDblAttr(this, "Start");
    
    private RoVecDblAttr? _PStart;
    public RoVecDblAttr PStart => this._PStart ??= new RoVecDblAttr(this, "PStart");
    
    private RwVecIntAttr? _BranchPriority;
    public RwVecIntAttr BranchPriority => this._BranchPriority ??= new RwVecIntAttr(this, "BranchPriority");

    private RoVecBoolAttr? _PWLObjCvx;
    public RoVecBoolAttr PWLObjCvx => this._PWLObjCvx ??= new RoVecBoolAttr(this, "PWLObjCvx");
    
    private RoVecDblAttr? _VarHintVal;
    public RoVecDblAttr VarHintVal => this._VarHintVal ??= new RoVecDblAttr(this, "VarHintVal");

    private RoVecIntAttr? _VarHintPri;
    public RoVecIntAttr VarHintPri => this._VarHintPri ??= new RoVecIntAttr(this, "VarHintPri");

    private RoVecIntAttr? _Partition;
    public RoVecIntAttr Partition => this._Partition ??= new RoVecIntAttr(this, "Partition");

    private RoVecIntAttr? _PoolIgnore;
    public RoVecIntAttr PoolIgnore => this._PoolIgnore ??= new RoVecIntAttr(this, "PoolIgnore");

    private RwVecStrAttr? _VTag;
    public RwVecStrAttr VTag => this._VTag ??= new RwVecStrAttr(this, "VTag");

    // variable attributes (related to the current solution)
    
    private RoVecDblAttr? _X;
    public RoVecDblAttr X => this._X ??= new RoVecDblAttr(this, "X");
    
    private RoVecDblAttr? _Xn;
    public RoVecDblAttr Xn => this._Xn ??= new RoVecDblAttr(this, "Xn");
    
    private RoVecDblAttr? _Rc;
    public RoVecDblAttr Rc => this._Rc ??= new RoVecDblAttr(this, "RC");
    
    private RwVecVbAttr? _VBasis;
    public RwVecVbAttr VBasis => this._VBasis ??= new RwVecVbAttr(this, "VBasis");

    // constraint attributes
    
    private RwVecCsAttr? _Sense;
    public RwVecCsAttr Sense => this._Sense ??= new RwVecCsAttr(this, "Sense");
    
    private RwVecDblAttr? _Rhs;
    public RwVecDblAttr Rhs => this._Rhs ??= new RwVecDblAttr(this, "RHS");
    
    private RwVecStrAttr? _ConstrName;
    public RwVecStrAttr ConstrName => this._ConstrName ??= new RwVecStrAttr(this, "ConstrName");

    private RwVecStrAttr? _CTag;
    public RwVecStrAttr CTag => this._CTag ??= new RwVecStrAttr(this, "CTag");

    private RwVecDblAttr? _DStart;
    public RwVecDblAttr DStart => this._DStart ??= new RwVecDblAttr(this, "DStart");

    private RoVecBoolAttr? _Lazy;
    public RoVecBoolAttr Lazy => this._Lazy ??= new RoVecBoolAttr(this, "Lazy");

    // quadratic constraint attributes
    
    private RwVecCsAttr? _QCSense;
    public RwVecCsAttr QCSense => this._QCSense ??= new RwVecCsAttr(this, "QCSense");
    
    private RwVecDblAttr? _QCRhs;
    public RwVecDblAttr QCRhs => this._QCRhs ??= new RwVecDblAttr(this, "QCRHS");
    
    private RwVecStrAttr? _QCName;
    public RwVecStrAttr QCName => this._QCName ??= new RwVecStrAttr(this, "QCName");

    private RwVecStrAttr? _QCTag;
    public RwVecStrAttr QCTag => this._QCTag ??= new RwVecStrAttr(this, "QCTag");

    // general constraint attributes
    
    private RwVecIntAttr? _GenConstrType;
    public RwVecIntAttr GenConstrType => this._GenConstrType ??= new RwVecIntAttr(this, "GenConstrType");
    
    private RwVecStrAttr? _GenConstrName;
    public RwVecStrAttr GenConstrName => this._GenConstrName ??= new RwVecStrAttr(this, "GenConstrName");
    
    // constraint attributes (related to the current solution)
    
    private RoVecDblAttr? _Pi;
    public RoVecDblAttr Pi => this._Pi ??= new RoVecDblAttr(this, "Pi");
    
    private RoVecDblAttr? _Slack;
    public RoVecDblAttr Slack => this._Slack ??= new RoVecDblAttr(this, "Slack");

    private RwVecCbAttr? _CBasis;
    public RwVecCbAttr CBasis => this._CBasis ??= new RwVecCbAttr(this, "CBasis");

    private RoVecDblAttr? _QCPi;
    public RoVecDblAttr QCPi => this._QCPi ??= new RoVecDblAttr(this, "QCPi");

    private RoVecDblAttr? _QCSlack;
    public RoVecDblAttr QCSlack => this._QCSlack ??= new RoVecDblAttr(this, "QCSlack");

    private RoVecDblAttr? _CDualNorm;
    public RoVecDblAttr CDualNorm => this._CDualNorm ??= new RoVecDblAttr(this, "CDualNorm");

    // IIS attributes
    
    public bool IISMinimal => GetIntAttr(ObjRef, "IISMinimal") != 0;
    
    private RoVecBoolAttr? _IISLb;
    public RoVecBoolAttr IISLb => this._IISLb ??= new RoVecBoolAttr(this, "IISLB");
    
    private RoVecBoolAttr? _IISUb;
    public RoVecBoolAttr IISUb => this._IISUb ??= new RoVecBoolAttr(this, "IISUB");
    
    private RoVecBoolAttr? _IISConstr;
    public RoVecBoolAttr IISConstr => this._IISConstr ??= new RoVecBoolAttr(this, "IISConstr");
    
    private RoVecBoolAttr? _IISSos;
    public RoVecBoolAttr IISSos => this._IISSos ??= new RoVecBoolAttr(this, "IISSOS");
    
    private RoVecBoolAttr? _IISQConstr;
    public RoVecBoolAttr IISQConstr => this._IISQConstr ??= new RoVecBoolAttr(this, "IISQConstr");
    
    private RoVecBoolAttr? _IISGenConstr;
    public RoVecBoolAttr IISGenConstr => this._IISGenConstr ??= new RoVecBoolAttr(this, "IISGenConstr");

    // LP sensitivity analysis attributes
    
    private RoVecDblAttr? _SAObjLow;
    public RoVecDblAttr SAObjLow => this._SAObjLow ??= new RoVecDblAttr(this, "SAObjLow");

    private RoVecDblAttr? _SAObjUp;
    public RoVecDblAttr SAObjUp => this._SAObjUp ??= new RoVecDblAttr(this, "SAObjUp");

    private RoVecDblAttr? _SALbLow;
    public RoVecDblAttr SALbLow => this._SALbLow ??= new RoVecDblAttr(this, "SALbLow");

    private RoVecDblAttr? _SALbUp;
    public RoVecDblAttr SALbUp => this._SALbUp ??= new RoVecDblAttr(this, "SALbUp");

    private RoVecDblAttr? _SAUbLow;
    public RoVecDblAttr SAUbLow => this._SAUbLow ??= new RoVecDblAttr(this, "SAUbLow");

    private RoVecDblAttr? _SAUbUp;
    public RoVecDblAttr SAUbUp => this._SAUbUp ??= new RoVecDblAttr(this, "SAUbUp");
    
    private RoVecDblAttr? _SARhsLow;
    public RoVecDblAttr SARhsLow => this._SARhsLow ??= new RoVecDblAttr(this, "SARHSLow");

    private RoVecDblAttr? _SARhsUp;
    public RoVecDblAttr SARhsUp => this._SARhsUp ??= new RoVecDblAttr(this, "SARHSUp");

    // advanced simplex attributes

    private RoVecDblAttr? _UnbdRay;
    public RoVecDblAttr UnbdRay => this._UnbdRay ??= new RoVecDblAttr(this, "UnbdRay");

    private RoVecDblAttr? _FarkasDual;
    public RoVecDblAttr FarkasDual => this._FarkasDual ??= new RoVecDblAttr(this, "FarkasDual");
    
    public double FarkasProof => GetDblAttr(ObjRef, "FarkasProof");
    
    public int InfeasVar => GetIntAttr(ObjRef, "InfeasVar");
    
    public int UnbdVar => GetIntAttr(ObjRef, "UnbdVar");


    // solution quality attributes

    public double MaxVio => GetDblAttr(ObjRef, "MaxVio");

    private RoVecDblAttr? _BoundVio;
    public RoVecDblAttr BoundVio => this._BoundVio ??= new RoVecDblAttr(this, "BoundVio");

    private RoVecDblAttr? _BoundSVio;
    public RoVecDblAttr BoundSVio => this._BoundSVio ??= new RoVecDblAttr(this, "BoundSVio");

    private RoVecIntAttr? _BoundVioIndex;
    public RoVecIntAttr BoundVioIndex => this._BoundVioIndex ??= new RoVecIntAttr(this, "BoundVioIndex");

    private RoVecIntAttr? _BoundSVioIndex;
    public RoVecIntAttr BoundSVioIndex => this._BoundSVioIndex ??= new RoVecIntAttr(this, "BoundSVioIndex");

    private RoVecDblAttr? _BoundVioSum;
    public RoVecDblAttr BoundVioSum => this._BoundVioSum ??= new RoVecDblAttr(this, "BoundVioSum");

    private RoVecDblAttr? _BoundSVioSum;
    public RoVecDblAttr BoundSVioSum => this._BoundSVioSum ??= new RoVecDblAttr(this, "BoundSVioSum");

    private RoVecDblAttr? _ConstrVio;
    public RoVecDblAttr ConstrVio => this._ConstrVio ??= new RoVecDblAttr(this, "ConstrVio");

    private RoVecDblAttr? _ConstrSVio;
    public RoVecDblAttr ConstrSVio => this._ConstrSVio ??= new RoVecDblAttr(this, "ConstrSVio");

    private RoVecIntAttr? _ConstrVioIndex;
    public RoVecIntAttr ConstrVioIndex => this._ConstrVioIndex ??= new RoVecIntAttr(this, "ConstrVioIndex");

    private RoVecIntAttr? _ConstrSVioIndex;
    public RoVecIntAttr ConstrSVioIndex => this._ConstrSVioIndex ??= new RoVecIntAttr(this, "ConstrSVioIndex");

    private RoVecDblAttr? _ConstrVioSum;
    public RoVecDblAttr ConstrVioSum => this._ConstrVioSum ??= new RoVecDblAttr(this, "ConstrVioSum");

    private RoVecDblAttr? _ConstrSVioSum;
    public RoVecDblAttr ConstrSVioSum => this._ConstrSVioSum ??= new RoVecDblAttr(this, "ConstrSVioSum");

    private RoVecDblAttr? _ConstrResidual;
    public RoVecDblAttr ConstrResidual => this._ConstrResidual ??= new RoVecDblAttr(this, "ConstrResidual");

    private RoVecDblAttr? _ConstrSResidual;
    public RoVecDblAttr ConstrSResidual => this._ConstrSResidual ??= new RoVecDblAttr(this, "ConstrSResidual");

    private RoVecIntAttr? _ConstrResidualIndex;
    public RoVecIntAttr ConstrResidualIndex => this._ConstrResidualIndex ??= new RoVecIntAttr(this, "ConstrResidualIndex");

    private RoVecIntAttr? _ConstrSResidualIndex;
    public RoVecIntAttr ConstrSResidualIndex => this._ConstrSResidualIndex ??= new RoVecIntAttr(this, "ConstrSResidualIndex");

    private RoVecDblAttr? _ConstrResidualSum;
    public RoVecDblAttr ConstrResidualSum => this._ConstrResidualSum ??= new RoVecDblAttr(this, "ConstrResidualSum");

    private RoVecDblAttr? _ConstrSResidualSum;
    public RoVecDblAttr ConstrSResidualSum => this._ConstrSResidualSum ??= new RoVecDblAttr(this, "ConstrSResidualSum");

    private RoVecDblAttr? _DualVio;
    public RoVecDblAttr DualVio => this._DualVio ??= new RoVecDblAttr(this, "DualVio");

    private RoVecDblAttr? _DualSVio;
    public RoVecDblAttr DualSVio => this._DualSVio ??= new RoVecDblAttr(this, "DualSVio");

    private RoVecIntAttr? _DualVioIndex;
    public RoVecIntAttr DualVioIndex => this._DualVioIndex ??= new RoVecIntAttr(this, "DualVioIndex");

    private RoVecIntAttr? _DualSVioIndex;
    public RoVecIntAttr DualSVioIndex => this._DualSVioIndex ??= new RoVecIntAttr(this, "DualSVioIndex");

    private RoVecDblAttr? _DualVioSum;
    public RoVecDblAttr DualVioSum => this._DualVioSum ??= new RoVecDblAttr(this, "DualVioSum");

    private RoVecDblAttr? _DualSVioSum;
    public RoVecDblAttr DualSVioSum => this._DualSVioSum ??= new RoVecDblAttr(this, "DualSVioSum");

    private RoVecDblAttr? _DualResidual;
    public RoVecDblAttr DualResidual => this._DualResidual ??= new RoVecDblAttr(this, "DualResidual");

    private RoVecDblAttr? _DualSResidual;
    public RoVecDblAttr DualSResidual => this._DualSResidual ??= new RoVecDblAttr(this, "DualSResidual");

    private RoVecIntAttr? _DualResidualIndex;
    public RoVecIntAttr DualResidualIndex => this._DualResidualIndex ??= new RoVecIntAttr(this, "DualResidualIndex");

    private RoVecIntAttr? _DualSResidualIndex;
    public RoVecIntAttr DualSResidualIndex => this._DualSResidualIndex ??= new RoVecIntAttr(this, "DualSResidualIndex");

    private RoVecDblAttr? _DualResidualSum;
    public RoVecDblAttr DualResidualSum => this._DualResidualSum ??= new RoVecDblAttr(this, "DualResidualSum");

    private RoVecDblAttr? _DualSResidualSum;
    public RoVecDblAttr DualSResidualSum => this._DualSResidualSum ??= new RoVecDblAttr(this, "DualSResidualSum");

    private RoVecDblAttr? _ComplVio;
    public RoVecDblAttr ComplVio => this._ComplVio ??= new RoVecDblAttr(this, "ComplVio");

    private RoVecIntAttr? _ComplVioIndex;
    public RoVecIntAttr ComplVioIndex => this._ComplVioIndex ??= new RoVecIntAttr(this, "ComplVioIndex");

    private RoVecDblAttr? _ComplVioSum;
    public RoVecDblAttr ComplVioSum => this._ComplVioSum ??= new RoVecDblAttr(this, "ComplVioSum");

    private RoVecDblAttr? _IntVio;
    public RoVecDblAttr IntVio => this._IntVio ??= new RoVecDblAttr(this, "IntVio");

    private RoVecIntAttr? _IntVioIndex;
    public RoVecIntAttr IntVioIndex => this._IntVioIndex ??= new RoVecIntAttr(this, "IntVioIndex");

    private RoVecDblAttr? _IntVioSum;
    public RoVecDblAttr IntVioSum => this._IntVioSum ??= new RoVecDblAttr(this, "IntVioSum");

    public double Kappa => GetDblAttr(ObjRef, "Kappa");

    public double KappaExact => GetDblAttr(ObjRef, "KappaExact");

    public double N2Kappa => GetDblAttr(ObjRef, "N2Kappa");
}
