
//  
//  NativeMethods.cs
//  gurobi-sharp
//
//  Copyright 2009-2024 Markus Uhr <uhrmar@gmail.com>.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in
//        the documentation and/or other materials provided with the
//        distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.Marshalling;

namespace Grb;

[CustomMarshaller(typeof(string), MarshalMode.ManagedToUnmanagedOut, typeof(LibraryOwnedUtf8StringMarshaller))]
internal unsafe static class LibraryOwnedUtf8StringMarshaller
{
    public static string? ConvertToManaged(byte* val) => Utf8StringMarshaller.ConvertToManaged(val);
}

internal partial class NativeMethods
{

    // Environment Creation and Destruction

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBloadenv(out IntPtr env, string? logfilename);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void GRBfreeenv(IntPtr env);

    // Model Creation and Modification

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBloadmodel(
        IntPtr env,
        out IntPtr model,
        string name,
        int numvars,
        int numconstrs,
        int objsense,
        double objcon,
        [In] double[] obj,
        [In] ConstrSense[] sense,
        [In] double[] rhs,
        [In] int[] vbeg,
        [In] int[] vlen,
        [In] int[] vind,
        [In] double[] vval,
        [In] double[] lb,
        [In] double[] ub,
        [In] VariableType[] vtype,
        [In] string[] varnames,
        [In] string?[]? constrnames);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBnewmodel(
        IntPtr env,
        out IntPtr model,
        string? name,
        int numvars,
        [In] double[]? obj,
        [In] double[]? lb,
        [In] double[]? ub,
        [In] byte[]? vtype,
        [In] string?[]? varnames);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr GRBcopymodel(IntPtr model);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBupdatemodel(IntPtr model);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBfreemodel(IntPtr model);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBaddvar(
        IntPtr model,
        int numnz,
        in int idx,
        in double val,
        double obj,
        double lb,
        double ub,
        VariableType vtype,
        string? name);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBaddvars(
        IntPtr model,
        int numvars,
        int numnz,
        [In] int[]? ofs,
        [In] int[]? idx,
        [In] double[]? val,
        [In] double[] obj,
        [In] double[] lb,
        [In] double[] ub,
        [In] VariableType[] vtype,
        [In] string?[]? varname);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBaddconstr(
        IntPtr model,
        int numnz,
        in int idx,
        in double val,
        ConstrSense sense,
        double rhs,
        string? constrname);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBaddconstrs(
        IntPtr model,
        int numconstrs,
        int numnz,
        [In] int[]? ofs,
        [In] int[]? idx,
        [In] double[]? val,
        [In] ConstrSense[] sense,
        [In] double[] rhs,
        [In] string?[]? constrnames);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBaddrangeconstr(
        IntPtr model,
        int numnz,
        in int idx,
        in double val,
        double lower,
        double upper,
        string? constrname);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBaddrangeconstrs(
        IntPtr model,
        int numconstrs,
        int numnz,
        [In] int[] ofs,
        [In] int[] idx,
        [In] double[] val,
        [In] double[] lower,
        [In] double[] upper,
        [In] string?[]? constrnames);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBaddsos(IntPtr model, int numsos, int nummembers, in SosType types, in int beg, in int ind, in double weight);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBdelvars(IntPtr model, int numdel, in int ind);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBdelconstrs(IntPtr model, int numdel, in int ind);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBdelsos(IntPtr model, int len, in int ind);

    // Model Solution

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBoptimize(IntPtr model);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBoptimizeasync(IntPtr model);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBsync(IntPtr model);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBcomputeIIS(IntPtr model);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBreset(IntPtr model, int clearall);

    // Model Queries

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr GRBgetenv(IntPtr model);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBgetcoeff(IntPtr model, int cstrind, int varind, out double val);

    // Input/Output

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBreadmodel(IntPtr env, string filename, out IntPtr model);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBread(IntPtr model, string filename);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBwrite(IntPtr model, string filename);

    // Attribute Management

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBgetattrinfo(IntPtr model, string attrname, out AttrDatatype datatype, out AttrType attrtype, out int settable);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBisattravailable(IntPtr model, string attrname);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBgetintattr(IntPtr model, string name, out int val);
    
    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBgetintattrelement(IntPtr model, string name, int index, out int val);
    
    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBsetintattr(IntPtr model, string name, int val);
    
    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBsetintattrelement(IntPtr model, string name, int index, int val);
    
    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBgetdblattr(IntPtr model, string name, out double val);
    
    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBgetdblattrelement(IntPtr model, string name, int index, out double val);
    
    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBsetdblattr(IntPtr model, string name, double val);
    
    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBsetdblattrelement(IntPtr model, string name, int index, double val);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBgetcharattrelement(IntPtr model, string name, int index, out byte c);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBsetcharattrelement(IntPtr model, string name, int index, byte c);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBgetstrattr(IntPtr model, string name, [MarshalUsing(typeof(LibraryOwnedUtf8StringMarshaller))] out string? val);
    
    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBgetstrattrelement(IntPtr model, string name, int index, [MarshalUsing(typeof(LibraryOwnedUtf8StringMarshaller))] out string? val);
    
    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBsetstrattr(IntPtr model, string name, string? val);
    
    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBsetstrattrelement(IntPtr model, string name, int index, string? val);

    // Parameter Management and Tuning

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBgetintparam(IntPtr env, string name, out int val);
    
    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBgetdblparam(IntPtr env, string name, out double val);
    
    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBgetstrparam(IntPtr model, string name, [MarshalUsing(typeof(LibraryOwnedUtf8StringMarshaller))] out string? val);
    
    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBsetintparam(IntPtr env, string name, int val);
    
    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBsetdblparam(IntPtr env, string name, double val);
    
    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBsetstrparam(IntPtr env, string name, string? val);

    // Monitoring Progress - Logging and Callbacks

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBversion(out int major, out int minor, out int technical);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBsetcallbackfunc(IntPtr model, IntPtr cb, IntPtr usrdata);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBcbget(IntPtr cbdata, int where, int what, out int result);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBcbget(IntPtr cbdata, int where, int what, out double result);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBcbget(IntPtr cbdata, int where, int what, double[] result);

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int GRBcbget(IntPtr cbdata, int where, int what, out IntPtr result);

    // Error Handling

    [LibraryImport("gurobi", StringMarshalling = StringMarshalling.Utf8)]
    [return: MarshalUsing(typeof(LibraryOwnedUtf8StringMarshaller))]
    internal static partial string? GRBgeterrormsg(IntPtr env);
}
