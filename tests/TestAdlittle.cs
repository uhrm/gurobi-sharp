using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using NUnit.Framework;

namespace Grb.Tests
{
    [TestFixture]
    public class TestAdlittle : TestLp
    {
        [Test]
        public void TestSolve()
        {
            using (var env = CreateTestEnvironment())
            using (var mod = LoadMps(env, "res/adlittle.mps"))
            {
                mod.Optimize();

                Assert.AreEqual(mod.Status, ModelStatus.Optimal);

                // this is from the NETLIB readme
                Assert.AreEqual(2.2549496316e5, mod.ObjVal, 1e-5);

                CheckKKT(mod, 1e-5);
            }
        }
    }
}
