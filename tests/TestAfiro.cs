using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using NUnit.Framework;

namespace Grb.Tests
{
    [TestFixture]
    public class TestAfiro : TestLp
    {
        [Test]
        public void TestSolve()
        {
            using (var env = CreateTestEnvironment())
            using (var mod = LoadMps(env, "res/afiro.mps"))
            {
                Assert.AreEqual(27, mod.NumConstrs);
                Assert.AreEqual(32, mod.NumVars);

                mod.Optimize();

                Assert.AreEqual(mod.Status, ModelStatus.Optimal);

                // this is from the NETLIB readme
                Assert.AreEqual(-4.6475314286e2, mod.ObjVal, 1e-8);

                CheckKKT(mod, 1e-8);
            }
        }
    }
}
