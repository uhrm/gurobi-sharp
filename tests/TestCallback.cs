using System;
using System.IO;
using NUnit.Framework;

namespace Grb.Tests
{
    [TestFixture]
    public class TestCallback : TestLp
    {
        private int MipNodeCb(Model model, MipNodeInfo info, TextWriter? tw)
        {
            if (tw == null) {
                return -1;
            }

            var n = model.NumVars;

            var x = new double[n];
            if (info.Status == ModelStatus.Optimal)
            {
                info.Rel(x);
            }
            else
            {
                for (var j=0; j<n; j++)
                    x[j] = Double.NaN;
            }

            tw.Write("{0}\t{1}\t{2}", info.NodCnt, info.SolCnt, info.ObjBnd);
//            for (var j=0; j<n; j++)
//                tw.Write("\t{0}", x[j]);
            tw.WriteLine();

//            Console.WriteLine("*** node {0,5}: status = {1},  sol count = {2}", data.NodCnt(), data.Status(), data.SolCnt());

            return 0;
        }

        private int MipCb(Model mode, MipInfo info, TextWriter? tw)
        {
            if (tw == null) {
                return -1;
            }

            tw.Write("***");
            tw.Write(" node {0,5}:", info.NodCnt);
            tw.Write(" sol count = {0}", info.SolCnt);
            tw.Write(" itr count = {0}", info.ItrCnt);
            tw.Write(" obj bound = {0:0.0000,10}", info.ObjBnd);
            tw.WriteLine();
            return 0;
        }

        [Test]
        public void TestMipNodeCallback()
        {
            using (var env = CreateTestEnvironment(threads: 1, nodeLimit: 1000))
            using (var mod = LoadMps(env, "res/p010_d107_1.mps"))
            {
                var n = mod.NumVars;

                using (var tw = new StreamWriter(Path.Combine(Path.GetTempPath(), "p010_d107_1_mipnode.tsv")))
                {
                    tw.Write("num\tobjbnd");
                    // for (var j=0; j<n; j++)
                    // {
                    //     var jname = this.mod.VarName[j];
                    //     if (String.IsNullOrEmpty(jname))
                    //         jname = String.Format("{0}", j);
                    //     tw.Write("\t{0}", jname);
                    // }
                    tw.WriteLine();

                    mod.RegisterCallback(mipnode: new MipNodeCallback<TextWriter>(MipNodeCb), userdata: tw);

                    mod.Optimize();
                }
            }
        }

        [Test]
        public void TestMipCallback()
        {
            using (var env = CreateTestEnvironment(threads: 1, nodeLimit: 1000))
            using (var mod = LoadMps(env, "res/p010_d107_1.mps"))
            {
                var n = mod.NumVars;

                using (var tw = new StreamWriter(Path.Combine(Path.GetTempPath(), "p010_d107_1_mip.tsv")))
                {
                    tw.Write("num\tobjbnd");
                    // for (var j=0; j<n; j++)
                    // {
                    //     var jname = this.mod.VarName[j];
                    //     if (String.IsNullOrEmpty(jname))
                    //         jname = String.Format("{0}", j);
                    //     tw.Write("\t{0}", jname);
                    // }
                    tw.WriteLine();

                    mod.RegisterCallback(mip: new MipCallback<TextWriter>(MipCb), userdata: tw);

                    mod.Optimize();
                }
            }
        }
    }
}
