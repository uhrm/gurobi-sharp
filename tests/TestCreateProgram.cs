using System;
using NUnit.Framework;

namespace Grb.Tests
{
    //   Minimize:
    // 
    //       x1 + 4 * x2 + 9 * x3
    // 
    //   Subject to:
    // 
    //       x1     + x2          <=  5
    //       x1              + x3 >= 10
    //              - x2     + x3 =   7
    // 
    //   With:
    // 
    //     0 <= x1 <= 4
    //    -1 <= x2 <= 1
    //          x3 unrestricted.

    [TestFixture]
    public class TestCreateProgram : TestLp
	{
        [Test]
        public void TestCreateSingle()
        {
            using (var env = CreateTestEnvironment())
            using (var mod = new Model(env, "small_program"))
            {
                mod.AddVar(1.0,  0.0, 4.0, VariableType.Continuous, "x1");
                mod.AddVar(4.0, -1.0, 1.0, VariableType.Continuous, "x2");
                mod.AddVar(9.0,  Double.NegativeInfinity, Double.PositiveInfinity, VariableType.Continuous, "x3");

                mod.Update();

                mod.AddConstr(new int[] {0, 1}, new double[] { 1.0, 1.0}, ConstrSense.LessEqual,     5.0, "ub");
                mod.AddConstr(new int[] {0, 2}, new double[] { 1.0, 1.0}, ConstrSense.GreaterEqual, 10.0, "lb");
                mod.AddConstr(new int[] {1, 2}, new double[] {-1.0, 1.0}, ConstrSense.Equal,         7.0, "eq");

                mod.Update();

                Assert.AreEqual(3, mod.NumVars);
                Assert.AreEqual(3, mod.NumConstrs);

                Assert.AreEqual("small_program", mod.ModelName);

                Assert.AreEqual(1.0, mod.Obj[0], 1e-10);
                Assert.AreEqual(4.0, mod.Obj[1], 1e-10);
                Assert.AreEqual(9.0, mod.Obj[2], 1e-10);

                Assert.AreEqual( 0.0, mod.Lb[0], 1e-10);
                Assert.AreEqual(-1.0, mod.Lb[1], 1e-10);
                Assert.AreEqual( 4.0, mod.Ub[0], 1e-10);
                Assert.AreEqual( 1.0, mod.Ub[1], 1e-10);

                Assert.AreEqual( 1.0, mod.GetCoeff(0,0), 1e-10);
                Assert.AreEqual( 1.0, mod.GetCoeff(0,1), 1e-10);
                Assert.AreEqual( 0.0, mod.GetCoeff(0,2), 1e-10);

                Assert.AreEqual( 1.0, mod.GetCoeff(1,0), 1e-10);
                Assert.AreEqual( 0.0, mod.GetCoeff(1,1), 1e-10);
                Assert.AreEqual( 1.0, mod.GetCoeff(1,2), 1e-10);

                Assert.AreEqual( 0.0, mod.GetCoeff(2,0), 1e-10);
                Assert.AreEqual(-1.0, mod.GetCoeff(2,1), 1e-10);
                Assert.AreEqual( 1.0, mod.GetCoeff(2,2), 1e-10);

                Assert.AreEqual( 5.0, mod.Rhs[0], 1e-10);
                Assert.AreEqual(10.0, mod.Rhs[1], 1e-10);
                Assert.AreEqual( 7.0, mod.Rhs[2], 1e-10);

                Assert.AreEqual(VariableType.Continuous, mod.VType[0]);
                Assert.AreEqual(VariableType.Continuous, mod.VType[1]);
                Assert.AreEqual(VariableType.Continuous, mod.VType[2]);

                Assert.AreEqual("x1", mod.VarName[0]);
                Assert.AreEqual("x2", mod.VarName[1]);
                Assert.AreEqual("x3", mod.VarName[2]);

                Assert.AreEqual(ConstrSense.LessEqual, mod.Sense[0]);
                Assert.AreEqual(ConstrSense.GreaterEqual, mod.Sense[1]);
                Assert.AreEqual(ConstrSense.Equal, mod.Sense[2]);

                Assert.AreEqual("ub", mod.ConstrName[0]);
                Assert.AreEqual("lb", mod.ConstrName[1]);
                Assert.AreEqual("eq", mod.ConstrName[2]);
            }
        }
        
        [Test]
        public void TestCreateMultiple()
        {
            using (var env = CreateTestEnvironment())
            using (var mod = new Model(env, "small_program"))
            {
                // mod.AddVar(1.0,  0.0, 4.0, VariableType.Continuous, "x1");
                // mod.AddVar(4.0, -1.0, 1.0, VariableType.Continuous, "x2");
                // mod.AddVar(9.0,  Double.NegativeInfinity, Double.PositiveInfinity, VariableType.Continuous, "x3");
                mod.AddVars(
                    new double[] { 1.0, 4.0, 9.0 },
                    new double[] { 0.0, -1.0, Double.NegativeInfinity },
                    new double[] { 4.0, 1.0, Double.PositiveInfinity },
                    new VariableType[] { VariableType.Continuous, VariableType.Continuous, VariableType.Continuous },
                    new string[] { "x1", "x2", "x3" }
                );

                mod.Update();

                // mod.AddConstr(new int[] {0, 1}, new double[] { 1.0, 1.0}, ConstrSense.LessEqual,     5.0, "ub");
                // mod.AddConstr(new int[] {0, 2}, new double[] { 1.0, 1.0}, ConstrSense.GreaterEqual, 10.0, "lb");
                // mod.AddConstr(new int[] {1, 2}, new double[] {-1.0, 1.0}, ConstrSense.Equal,         7.0, "eq");
                mod.AddConstrs(
                    new int[] { 0, 2, 4, 6 },
                    new int[] { 0, 1, 0, 2, 1, 2 },
                    new double[] { 1.0, 1.0, 1.0, 1.0, -1.0, 1.0 },
                    new ConstrSense[] { ConstrSense.LessEqual, ConstrSense.GreaterEqual, ConstrSense.Equal },
                    new double[] { 5.0, 10.0, 7.0 },
                    new string[] { "ub", "lb", "eq" });

                mod.Update();

                Assert.AreEqual(3, mod.NumVars);
                Assert.AreEqual(3, mod.NumConstrs);

                Assert.AreEqual("small_program", mod.ModelName);

                Assert.AreEqual(1.0, mod.Obj[0], 1e-10);
                Assert.AreEqual(4.0, mod.Obj[1], 1e-10);
                Assert.AreEqual(9.0, mod.Obj[2], 1e-10);

                Assert.AreEqual( 0.0, mod.Lb[0], 1e-10);
                Assert.AreEqual(-1.0, mod.Lb[1], 1e-10);
                Assert.AreEqual( 4.0, mod.Ub[0], 1e-10);
                Assert.AreEqual( 1.0, mod.Ub[1], 1e-10);

                Assert.AreEqual( 1.0, mod.GetCoeff(0,0), 1e-10);
                Assert.AreEqual( 1.0, mod.GetCoeff(0,1), 1e-10);
                Assert.AreEqual( 0.0, mod.GetCoeff(0,2), 1e-10);

                Assert.AreEqual( 1.0, mod.GetCoeff(1,0), 1e-10);
                Assert.AreEqual( 0.0, mod.GetCoeff(1,1), 1e-10);
                Assert.AreEqual( 1.0, mod.GetCoeff(1,2), 1e-10);

                Assert.AreEqual( 0.0, mod.GetCoeff(2,0), 1e-10);
                Assert.AreEqual(-1.0, mod.GetCoeff(2,1), 1e-10);
                Assert.AreEqual( 1.0, mod.GetCoeff(2,2), 1e-10);

                Assert.AreEqual( 5.0, mod.Rhs[0], 1e-10);
                Assert.AreEqual(10.0, mod.Rhs[1], 1e-10);
                Assert.AreEqual( 7.0, mod.Rhs[2], 1e-10);

                Assert.AreEqual(VariableType.Continuous, mod.VType[0]);
                Assert.AreEqual(VariableType.Continuous, mod.VType[1]);
                Assert.AreEqual(VariableType.Continuous, mod.VType[2]);

                Assert.AreEqual("x1", mod.VarName[0]);
                Assert.AreEqual("x2", mod.VarName[1]);
                Assert.AreEqual("x3", mod.VarName[2]);

                Assert.AreEqual(ConstrSense.LessEqual, mod.Sense[0]);
                Assert.AreEqual(ConstrSense.GreaterEqual, mod.Sense[1]);
                Assert.AreEqual(ConstrSense.Equal, mod.Sense[2]);

                Assert.AreEqual("ub", mod.ConstrName[0]);
                Assert.AreEqual("lb", mod.ConstrName[1]);
                Assert.AreEqual("eq", mod.ConstrName[2]);
            }
        }
    }
}
