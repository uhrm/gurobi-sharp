using System;
using NUnit.Framework;

namespace Grb.Tests;

//   Minimize:
// 
//       x1 + 4 * x2 + 9 * x3
// 
//   Subject to:
// 
//       x1     + x2          <=  5
//       x1              + x3 >= 10
//              - x2     + x3 =   7
// 
//   With:
// 
//     0 <= x1 <= 4
//    -1 <= x2 <= 1
//          x3 unrestricted.

[TestFixture]
public class TestErrorHandling : TestLp
{
    [Test]
    public void TestErrors()
    {
        using var env = CreateTestEnvironment();
        using var mod = new Model(env, "error_test");

        mod.AddVar(1.0,  0.0, 4.0, VariableType.Continuous, "x1");
        mod.AddVar(4.0, -1.0, 1.0, VariableType.Continuous, "x2");
        mod.AddVar(9.0,  Double.NegativeInfinity, Double.PositiveInfinity, VariableType.Continuous, "x3");

        mod.Update();

        mod.AddConstr(new int[] {0, 1}, new double[] { 1.0, 1.0}, ConstrSense.LessEqual,     5.0, "ub");
        mod.AddConstr(new int[] {0, 2}, new double[] { 1.0, 1.0}, ConstrSense.GreaterEqual, 10.0, "lb");
        mod.AddConstr(new int[] {1, 2}, new double[] {-1.0, 1.0}, ConstrSense.Equal,         7.0, "eq");
        try {
            mod.AddConstr(new int[] {1, 3}, new double[] {-1.0, 1.0}, ConstrSense.Equal, 7.0, "err_idx");
        }
        catch (GurobiException ex) {
            Assert.AreEqual(10006, ex.ErrNo);
        }
        try {
            mod.AddConstr(new int[] {1, 2}, new double[] {-1.0, Double.NaN}, ConstrSense.Equal, 7.0, "err_val");
        }
        catch (GurobiException ex) {
            Assert.AreEqual(10003, ex.ErrNo);
        }
        try {
            mod.AddConstr(new int[] {1, 2}, new double[] {-1.0, 1.0}, ConstrSense.Equal, Double.NaN, "err_rhs");
        }
        catch (GurobiException ex) {
            Assert.AreEqual(10003, ex.ErrNo);
        }

        mod.Update();

        Assert.AreEqual(9.0, mod.Obj[2], 1e-10);
        try {
            var c3 = mod.Obj[3];
        }
        catch (GurobiException ex) {
            Assert.AreEqual(10006, ex.ErrNo);
        }
    }
}
