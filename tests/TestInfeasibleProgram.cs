using System;
using System.IO;
using NUnit.Framework;

namespace Grb.Tests
{
    //   Minimize:
    // 
    //       x1 + x2 + x3
    // 
    //   Subject to:
    // 
    //       x1     + x2          >= 10
    //       x1              + x3 <=  4
    //                x2     + x3 >=  4
    //                x2     + x3 <=  5
    // 
    //   With:
    // 
    //     0 <= x1
    //     0 <= x2
    //     0 <= x3 <=3.

    [TestFixture]
    public class TestInfeasibleProgram : TestLp
    {
        private Model CreateModel(Environment env)
        {
            var mod = new Model(env, "infeasible_program");

            mod.AddVar(1.0,  0.0, Double.PositiveInfinity, VariableType.Continuous, "x1");
            mod.AddVar(1.0,  0.0, Double.PositiveInfinity, VariableType.Continuous, "x2");
            mod.AddVar(1.0,  0.0,                     3.0, VariableType.Continuous, "x3");

            mod.Update();

            mod.AddConstr(new int[] {0, 1}, new double[] {1.0, 1.0}, ConstrSense.GreaterEqual, 10.0);
            mod.AddConstr(new int[] {0, 2}, new double[] {1.0, 1.0}, ConstrSense.LessEqual,     4.0);
            mod.AddConstr(new int[] {1, 2}, new double[] {1.0, 1.0}, ConstrSense.GreaterEqual,  4.0);
            mod.AddConstr(new int[] {1, 2}, new double[] {1.0, 1.0}, ConstrSense.LessEqual,     5.0);

            mod.Update();
            return mod;
        }

        [Test]
        public void TestCreate()
        {
            using (var env = CreateTestEnvironment())
            using (var mod = CreateModel(env))
            {
                Assert.AreEqual(3, mod.NumVars);
                Assert.AreEqual(4, mod.NumConstrs);

                Assert.AreEqual(1.0, mod.Obj[0], 1e-10);
                Assert.AreEqual(1.0, mod.Obj[1], 1e-10);
                Assert.AreEqual(1.0, mod.Obj[2], 1e-10);

                Assert.AreEqual( 0.0, mod.Lb[0], 1e-10);
                Assert.AreEqual( 0.0, mod.Lb[1], 1e-10);
                Assert.AreEqual( 0.0, mod.Lb[2], 1e-10);
                Assert.AreEqual( 3.0, mod.Ub[2], 1e-10);

                Assert.AreEqual( 1.0, mod.GetCoeff(0,0), 1e-10);
                Assert.AreEqual( 1.0, mod.GetCoeff(0,1), 1e-10);
                Assert.AreEqual( 0.0, mod.GetCoeff(0,2), 1e-10);

                Assert.AreEqual( 1.0, mod.GetCoeff(1,0), 1e-10);
                Assert.AreEqual( 0.0, mod.GetCoeff(1,1), 1e-10);
                Assert.AreEqual( 1.0, mod.GetCoeff(1,2), 1e-10);

                Assert.AreEqual( 0.0, mod.GetCoeff(2,0), 1e-10);
                Assert.AreEqual( 1.0, mod.GetCoeff(2,1), 1e-10);
                Assert.AreEqual( 1.0, mod.GetCoeff(2,2), 1e-10);

                Assert.AreEqual( 0.0, mod.GetCoeff(3,0), 1e-10);
                Assert.AreEqual( 1.0, mod.GetCoeff(3,1), 1e-10);
                Assert.AreEqual( 1.0, mod.GetCoeff(3,2), 1e-10);

                Assert.AreEqual(10.0, mod.Rhs[0], 1e-10);
                Assert.AreEqual( 4.0, mod.Rhs[1], 1e-10);
                Assert.AreEqual( 4.0, mod.Rhs[2], 1e-10);
                Assert.AreEqual( 5.0, mod.Rhs[3], 1e-10);
            }
        }
        
        [Test]
        public void TestSolve()
        {
            using (var env = CreateTestEnvironment())
            using (var mod = CreateModel(env))
            {
                // suppress solver output
                mod.Environment.OutputFlag = 0;

                // compute infeasibility certificate
                mod.Environment.InfUnbdInfo = 1;

                mod.Optimize();

                Assert.AreEqual(mod.Status, ModelStatus.Infeasible);
            }
        }
        
        [Test]
        public void TestKKT()
        {
            using (var env = CreateTestEnvironment())
            using (var mod = CreateModel(env))
            {
                // compute infeasibility certificate
                mod.Environment.InfUnbdInfo = 1;

                mod.Optimize();

                // ***DEBUG***
                // Console.WriteLine();
                // for (var i=0; i<mod.NumConstrs; i++)
                // {
                //     Console.WriteLine("*** pi[{0}] = {1,7:0.0000}", i, mod.Pi[i]);
                // }
                // for (var i=0; i<mod.NumVars; i++)
                // {
                //     Console.WriteLine("*** rc[{0}] = {1,7:0.0000} ({2})", i, mod.Rc[i], mod.VBasis[i]);
                // }
                // for (var i=0; i<mod.NumConstrs; i++)
                // {
                //     Console.WriteLine("*** fd[{0}] = {1,7:0.0000}", i, mod.FarkasDual[i]);
                // }
                // // ***ENDEBUG***

                // check dual feasibility
                for (var j=0; j<mod.NumVars; j++) {
                    var res = mod.Obj[j];
                    for (var i=0; i<mod.NumConstrs; i++) {
                        var pi = mod.Pi[i];
                        switch (mod.Sense[i])
                        {
                        case ConstrSense.GreaterEqual:
                            Assert.IsTrue(pi > -1e-8, String.Format("Dual {0} of '>=' constraint is negative: {1}", i, pi));
                            break;
                        case ConstrSense.LessEqual:
                            Assert.IsTrue(pi <  1e-8, String.Format("Dual {0} of '<=' constraint is positive: {1}", i, pi));
                            break;
                        }
                        res -= pi*mod.GetCoeff(i,j);
                    }
                    var rc = mod.Rc[j];
                    switch (mod.VBasis[j])
                    {
                    case VariableStatus.Basic:
                        Assert.IsTrue(Math.Abs(rc) < 1e-8, String.Format("Reduced cost {0} of basic variable is non-zero: {1}", j, rc));
                        break;
                    case VariableStatus.NonbasicLower:
                        Assert.IsTrue(rc > -1e-8, String.Format("Reduced cost {0} of variable at lower bound is negative: {1}", j, rc));
                        break;
                    case VariableStatus.NonbasicUpper:
                        Assert.IsTrue(rc <  1e-8, String.Format("Reduced cost {0} of variable at upper bound is positive: {1}", j, rc));
                        break;
                    }
                    res -= rc;
                    Assert.AreEqual(0.0, res, 1e-8, String.Format("Infeasible dual constraint {0}: residual = {1}.", j, res));
                }

                // check dual unbounded ray
                var objray = 0.0;
                for (var i=0; i<mod.NumConstrs; i++) {
                    var ri = -mod.FarkasDual[i];
                    switch (mod.Sense[i])
                    {
                    case ConstrSense.GreaterEqual:
                        Assert.IsTrue(ri > -1e-8, String.Format("Farkas dual {0} of '>=' constraint is negative: {1}.", i, ri));
                        break;
                    case ConstrSense.LessEqual:
                        Assert.IsTrue(ri <  1e-8, String.Format("Farkas dual {0} of '<=' constraint is positive: {1}.", i, ri));
                        break;
                    }
                    objray += ri*mod.Rhs[i];
                }
                for (var j=0; j<mod.NumVars; j++) {
                    var res = 0.0;
                    for (var i=0; i<mod.NumConstrs; i++) {
                        var ri = -mod.FarkasDual[i];
                        res += ri*mod.GetCoeff(i,j);
                    }
                    if (res < -1e-8) {
                        // complete unbd ray with dual variable of lower bound j
                        Assert.AreEqual(VariableStatus.NonbasicLower, mod.VBasis[j]);
                        //Assert.AreEqual(mod.Rc[j], -res, 1e-8);
                        objray -= res*mod.Lb[j];
                    }
                    else if (res > 1e-8) {
                        // complete unbd ray with dual variable of upper bound j
                        Assert.AreEqual(VariableStatus.NonbasicUpper, mod.VBasis[j]);
                        //Assert.AreEqual(mod.Rc[j], -res, 1e-8);
                        objray -= res*mod.Ub[j];
                    }
                    // assuming res == 0.0
                    // else
                    // {
                    // }
                    Assert.IsTrue(objray >= 1e-8, String.Format("Nonpositive obj val of unbd ray: {0}.", objray));
                }
            }
        }
    }
}

