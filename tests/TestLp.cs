using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using NUnit.Framework;

namespace Grb.Tests
{
    public abstract class TestLp
    {
        protected Grb.Environment CreateTestEnvironment(int outputFlag = 0, int? threads = null, int? nodeLimit = null)
        {
            var env = new Environment();
            env.OutputFlag = outputFlag;
            if (threads.HasValue) {
                env.Threads = threads.Value;
            }
            if (nodeLimit.HasValue) {
                env.NodeLimit = nodeLimit.Value;
            }
            return env;
        }

        protected Grb.Model LoadMps(Grb.Environment env, string filename)
        {
            return Model.ReadModel(env, Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) ?? "", filename));
        }

        protected void CheckKKT(Grb.Model solver, double tol)
        {
            CheckKKT(solver, tol, 1e-8);
        }
        
        protected void CheckKKT(Grb.Model mod, double opttol, double zerotol)
        {
            // check dual feasibility
            for (var i=0; i<mod.NumConstrs; i++)
            {
                var pi = mod.Pi[i];
                switch (mod.Sense[i])
                {
                case ConstrSense.GreaterEqual:
                    Assert.IsTrue(pi > -zerotol, $"Invalid dual of '>=' constraint {i}: expected pi({i}) >= 0, found pi({i}) = {pi}");
                    break;
                case ConstrSense.LessEqual:
                    Assert.IsTrue(pi <  zerotol, $"Invalid dual of '<=' constraint {i}: expected pi({i}) <= 0, found pi({i}) = {pi}");
                    break;
                }
            }
            for (var j=0; j<mod.NumVars; j++)
            {
                var rc = mod.Rc[j];
                switch (mod.VBasis[j])
                {
                case VariableStatus.Basic:
                    Assert.IsTrue(Math.Abs(rc) < zerotol, $"Invalid reduced cost of basic variable {j}: expected rc({j}) = 0, found rc({j}) = {rc}");
                    break;
                case VariableStatus.NonbasicLower:
                    Assert.IsTrue(rc > -zerotol, $"Invalid reduced cost of variable {j} at lower bound: expected rc({j}) >= 0, found rc({j}) = {rc}");
                    break;
                case VariableStatus.NonbasicUpper:
                    Assert.IsTrue(rc <  zerotol, $"Invalid reduced cost of variable {j} at upper bound: expected rc({j}) <= 0, found rc({j}) = {rc}");
                    break;
                }
            }

            // check dual objective
            var dval = 0.0;
            for (var i=0; i<mod.NumConstrs; i++)
            {
                var bi = mod.Rhs[i];
                if (Math.Abs(bi) < 1e100)
                {
                    var pi = mod.Pi[i];
                    dval += pi*bi;
                }
            }
            for (var j=0; j<mod.NumVars; j++)
            {
                switch (mod.VBasis[j])
                {
                case VariableStatus.NonbasicLower:
                    var lb = mod.Lb[j];
                    if (lb > -1e100)
                    {
                        var rc = mod.Rc[j];
                        dval += rc*lb;
                    }
                    break;
                case VariableStatus.NonbasicUpper:
                    var ub = mod.Ub[j];
                    if (ub < 1e100)
                    {
                        var rc = mod.Rc[j];
                        dval += rc*ub;
                    }
                    break;
                }
            }
            Assert.AreEqual(mod.ObjVal, dval, opttol);

            // check complementary slackness
            for (var i=0; i<mod.NumConstrs; i++)
            {
                var ri = mod.Slack[i];
                var pi = mod.Pi[i];
                Assert.AreEqual(0.0, pi*ri, zerotol);
            }
            for (var j=0; j<mod.NumVars; j++)
            {
                var xj = mod.X[j];
                switch (mod.VBasis[j])
                {
                case VariableStatus.Basic:
                    var rc = mod.Rc[j];
                    Assert.AreEqual(0.0, rc, zerotol);
                    break;
                case VariableStatus.NonbasicLower:
                    var lb = mod.Lb[j];
                    Assert.AreEqual(0.0, lb-xj, zerotol);
                    break;
                case VariableStatus.NonbasicUpper:
                    var ub = mod.Ub[j];
                    Assert.AreEqual(0.0, ub-xj, zerotol);
                    break;
                default:
                    Assert.Fail($"Unexpected basis status of variable {j}: {mod.VBasis[j]}.");
                    break;
                }
            }
        }
    }
}
