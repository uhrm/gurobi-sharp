using System;
using System.IO;
using NUnit.Framework;

namespace Grb.Tests
{
    [TestFixture]
    public class TestSimpleProgram : TestLp
    {
        private Grb.Model CreateModel(Grb.Environment env)
        {
            // problem data
            var c = new double[] {-1.0, -5.0};
            var vn = new string[] {"x1", "x2"};
            
            var mod = new Grb.Model(env, "simple_program", 2, c, null, null, null, vn);
            
            mod.AddConstr(new int[] {0, 1}, new double[] {-2.0, 11.0}, ConstrSense.LessEqual,  99.0);
            mod.AddConstr(new int[] {0, 1}, new double[] {11.0,  4.0}, ConstrSense.LessEqual, 165.0);
            
            mod.Update();
            return mod;
        }
        
        [Test]
        public void TestDimensions()
        {
            using (var env = CreateTestEnvironment())
            using (var mod = CreateModel(env))
            {
                Assert.AreEqual(2, mod.NumConstrs);
                Assert.AreEqual(2, mod.NumVars);
            }
        }
        
        [Test]
        public void TestData()
        {
            using (var env = CreateTestEnvironment())
            using (var mod = CreateModel(env))
            {
                // objective
                Assert.AreEqual(-1.0, mod.Obj[0], 1e-10);
                Assert.AreEqual(-5.0, mod.Obj[1], 1e-10);
                
                // bounds
                Assert.AreEqual(0.0, mod.Lb[0], 1e-10);
                Assert.AreEqual(0.0, mod.Lb[1], 1e-10);

                // constraint matrix
                Assert.AreEqual( -2.0, mod.GetCoeff(0,0), 1e-10);
                Assert.AreEqual( 11.0, mod.GetCoeff(0,1), 1e-10);
                Assert.AreEqual( 11.0, mod.GetCoeff(1,0), 1e-10);
                Assert.AreEqual(  4.0, mod.GetCoeff(1,1), 1e-10);
                
                // constraint sense
                Assert.AreEqual(ConstrSense.LessEqual, mod.Sense[0]);
                Assert.AreEqual(ConstrSense.LessEqual, mod.Sense[1]);
                
                // constraint rhs
                Assert.AreEqual( 99.0, mod.Rhs[0], 1e-10);
                Assert.AreEqual(165.0, mod.Rhs[1], 1e-10);
            }
        }
        
        [Test]
        public void TestSolve()
        {
            using (var env = CreateTestEnvironment())
            using (var mod = CreateModel(env))
            {
                // suppress solver output
                mod.Environment.OutputFlag = 0;

                mod.Optimize();
                
                Assert.AreEqual(-66.0, mod.ObjVal, 1e-9);
                
                Assert.AreEqual(11.0, mod.X[0], 1e-9);
                Assert.AreEqual(11.0, mod.X[1], 1e-9);
            }
        }
    }
}
