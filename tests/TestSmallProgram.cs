using System;
using System.IO;
using NUnit.Framework;

namespace Grb.Tests
{
    //   Minimize:
    // 
    //       x1 + 4 * x2 + 9 * x3
    // 
    //   Subject to:
    // 
    //       x1     + x2          <=  5
    //       x1              + x3 >= 10
    //              - x2     + x3 =   7
    // 
    //   With:
    // 
    //     0 <= x1 <= 4
    //    -1 <= x2 <= 1
    //          x3 unrestricted.

    [TestFixture]
    public class TestSmallProgram : TestLp
	{
        private Grb.Model CreateModel(Grb.Environment env)
        {
            var mod = new Model(env, "small_program");

            mod.AddVar(1.0,  0.0, 4.0, VariableType.Continuous, "x1");
            mod.AddVar(4.0, -1.0, 1.0, VariableType.Continuous, "x2");
            mod.AddVar(9.0,  Double.NegativeInfinity, Double.PositiveInfinity, VariableType.Continuous, "x3");

            mod.Update();

            mod.AddConstr(new int[] {0, 1}, new double[] { 1.0, 1.0}, ConstrSense.LessEqual,     5.0, "ub");
            mod.AddConstr(new int[] {0, 2}, new double[] { 1.0, 1.0}, ConstrSense.GreaterEqual, 10.0, "lb");
            mod.AddConstr(new int[] {1, 2}, new double[] {-1.0, 1.0}, ConstrSense.Equal,         7.0, "eq");

            mod.Update();
            return mod;
        }
        
        [Test]
        public void TestCreate()
        {
            using (var env = CreateTestEnvironment())
            using (var mod = CreateModel(env))
            {
                Assert.AreEqual(3, mod.NumVars);
                Assert.AreEqual(3, mod.NumConstrs);

                Assert.AreEqual("small_program", mod.ModelName);

                Assert.AreEqual(1.0, mod.Obj[0], 1e-10);
                Assert.AreEqual(4.0, mod.Obj[1], 1e-10);
                Assert.AreEqual(9.0, mod.Obj[2], 1e-10);

                Assert.AreEqual( 0.0, mod.Lb[0], 1e-10);
                Assert.AreEqual(-1.0, mod.Lb[1], 1e-10);
                Assert.AreEqual( 4.0, mod.Ub[0], 1e-10);
                Assert.AreEqual( 1.0, mod.Ub[1], 1e-10);

                Assert.AreEqual( 1.0, mod.GetCoeff(0,0), 1e-10);
                Assert.AreEqual( 1.0, mod.GetCoeff(0,1), 1e-10);
                Assert.AreEqual( 0.0, mod.GetCoeff(0,2), 1e-10);

                Assert.AreEqual( 1.0, mod.GetCoeff(1,0), 1e-10);
                Assert.AreEqual( 0.0, mod.GetCoeff(1,1), 1e-10);
                Assert.AreEqual( 1.0, mod.GetCoeff(1,2), 1e-10);

                Assert.AreEqual( 0.0, mod.GetCoeff(2,0), 1e-10);
                Assert.AreEqual(-1.0, mod.GetCoeff(2,1), 1e-10);
                Assert.AreEqual( 1.0, mod.GetCoeff(2,2), 1e-10);

                Assert.AreEqual( 5.0, mod.Rhs[0], 1e-10);
                Assert.AreEqual(10.0, mod.Rhs[1], 1e-10);
                Assert.AreEqual( 7.0, mod.Rhs[2], 1e-10);

                Assert.AreEqual(VariableType.Continuous, mod.VType[0]);
                Assert.AreEqual(VariableType.Continuous, mod.VType[1]);
                Assert.AreEqual(VariableType.Continuous, mod.VType[2]);

                Assert.AreEqual("x1", mod.VarName[0]);
                Assert.AreEqual("x2", mod.VarName[1]);
                Assert.AreEqual("x3", mod.VarName[2]);

                Assert.AreEqual(ConstrSense.LessEqual, mod.Sense[0]);
                Assert.AreEqual(ConstrSense.GreaterEqual, mod.Sense[1]);
                Assert.AreEqual(ConstrSense.Equal, mod.Sense[2]);

                Assert.AreEqual("ub", mod.ConstrName[0]);
                Assert.AreEqual("lb", mod.ConstrName[1]);
                Assert.AreEqual("eq", mod.ConstrName[2]);
            }
        }
        
        [Test]
        public void TestSolve()
        {
            using (var env = CreateTestEnvironment())
            using (var mod = CreateModel(env))
            {
                mod.Optimize();

                Assert.AreEqual(mod.Status, ModelStatus.Optimal);
                
                Assert.AreEqual(54.0, mod.ObjVal, 1e-12);
                
                Assert.AreEqual( 4.0, mod.X[0], 1e-12);
                Assert.AreEqual(-1.0, mod.X[1], 1e-12);
                Assert.AreEqual( 6.0, mod.X[2], 1e-12);

                CheckKKT(mod, 1e-12);
            }
        }
        
        //[Test]
        public void TestKKT()
        {
            using (var env = CreateTestEnvironment())
            using (var mod = CreateModel(env))
            {
                // suppress solver output
                mod.Environment.OutputFlag = 0;

                mod.Optimize();

                // check dual feasibility
                for (var i=0; i<mod.NumConstrs; i++)
                {
                    var pi = mod.Pi[i];
                    switch (mod.Sense[i])
                    {
                    case ConstrSense.GreaterEqual:
                        Assert.IsTrue(pi > -1e-8, String.Format("Dual {0} of '>=' constraint is negative: {1}", i, pi));
                        break;
                    case ConstrSense.LessEqual:
                        Assert.IsTrue(pi <  1e-8, String.Format("Dual {0} of '<=' constraint is positive: {1}", i, pi));
                        break;
                    }
                }
                for (var j=0; j<mod.NumVars; j++)
                {
                    var rc = mod.Rc[j];
                    switch (mod.VBasis[j])
                    {
                    case VariableStatus.Basic:
                        Assert.IsTrue(Math.Abs(rc) < 1e-8, String.Format("Reduced cost {0} of basic variable is non-zero: {1}", j, rc));
                        break;
                    case VariableStatus.NonbasicLower:
                        Assert.IsTrue(rc > -1e-8, String.Format("Reduced cost {0} of variable at lower bound is negative: {1}", j, rc));
                        break;
                    case VariableStatus.NonbasicUpper:
                        Assert.IsTrue(rc <  1e-8, String.Format("Reduced cost {0} of variable at upper bound is positive: {1}", j, rc));
                        break;
                    }
                }

                // check dual objective
                var dval = 0.0;
                for (var i=0; i<mod.NumConstrs; i++)
                {
                    var bi = mod.Rhs[i];
                    if (Math.Abs(bi) < 1e100)
                    {
                        var pi = mod.Pi[i];
                        dval += pi*bi;
                    }
                }
                for (var j=0; j<mod.NumVars; j++)
                {
                    switch (mod.VBasis[j])
                    {
                    case VariableStatus.NonbasicLower:
                        var lb = mod.Lb[j];
                        if (lb > -1e100)
                        {
                            var rc = mod.Rc[j];
                            dval += rc*lb;
                        }
                        break;
                    case VariableStatus.NonbasicUpper:
                        var ub = mod.Ub[j];
                        if (ub < 1e100)
                        {
                            var rc = mod.Rc[j];
                            dval += rc*ub;
                        }
                        break;
                    }
                }
                Assert.AreEqual(mod.ObjVal, dval, 1e-8);
                
                // check complementary slackness
                for (var i=0; i<mod.NumConstrs; i++)
                {
                    var ri = mod.Slack[i];
                    var pi = mod.Pi[i];
                    Assert.AreEqual(0.0, pi*ri, 1e-8);
                }
                for (var j=0; j<mod.NumVars; j++)
                {
                    var xj = mod.X[j];
                    switch (mod.VBasis[j])
                    {
                    case VariableStatus.Basic:
                        var rc = mod.Rc[j];
                        Assert.AreEqual(0.0, rc, 1e-8);
                        break;
                    case VariableStatus.NonbasicLower:
                        var lb = mod.Lb[j];
                        Assert.AreEqual(0.0, lb-xj, 1e-8);
                        break;
                    case VariableStatus.NonbasicUpper:
                        var ub = mod.Ub[j];
                        Assert.AreEqual(0.0, ub-xj, 1e-8);
                        break;
                    default:
                        Assert.Fail(String.Format("Unexpected basis status of variable {0}: {1}.", j, mod.VBasis[j]));
                        break;
                    }
                }
            }
        }
    }
}
